cfg = {}

cfg.showself = false -- True: shows your own id and blip
cfg.distance = 15 -- Max distance for id

cfg.default = {r = 255, g = 255, b = 255} -- Colors for default id
cfg.talker = {r = 255, g = 255, b = 51} -- Colors for talker id

cfg.police = {}
cfg.police.show = true -- Show cops blips to other cops
cfg.police.distance = 300 -- Max distance for cops blips

cfg.police.id = {r = 70, g = 100, b = 200} -- Colors for cops id
cfg.police.sprite = 1 -- Sprite for cops blips
cfg.police.colour = 29 -- Colors for cops blips