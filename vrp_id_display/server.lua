local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
IDDclient = Tunnel.getInterface("vrp_id_display","vrp_id_display")
vRPclient = Tunnel.getInterface("vRP","vrp_id_display")
vRP = Proxy.getInterface("vRP")

AddEventHandler("vRP:playerLeave",function(user_id, source) 
	local users = vRP.getUsers({})
	for k,v in pairs(users) do
		IDDclient.removeUser(v,{user_id})
	end
end)

AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn) 
	local users = vRP.getUsers({})
	for k,v in pairs(users) do
	  IDDclient.insertUser(source,{k,v})
	  IDDclient.insertUser(v,{user_id,source})
	end
end)
