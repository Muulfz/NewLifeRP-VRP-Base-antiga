
local cfg = {}

-- define static item transformers
-- see https://github.com/ImagicTheCat/vRP to understand the item transformer concept/definition

cfg.item_transformers = {
  -- example of harvest item transformer
  --[[ {
    name="Trash Can", -- menu name
    -- permissions = {"harvest.water_bottle_tacos"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=10,
    units_per_minute=1,
    x=231.40283203125,y=-1507.09191894531,z=29.2916603088379, -- pos
    radius=5, height=1.5, -- area
    recipes = {
      ["Harvest water"] = { -- action name
        description="Harvest some water bottles.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["water"] = 1
        }
      },
      ["Harvest tacos"] = { -- action name
        description="Harvest some tacos.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["tacos"] = 1
        }
      }
    }
    --, onstart = function(player,recipe) end, -- optional start callback
    -- onstep = function(player,recipe) end, -- optional step callback
    -- onstop = function(player,recipe) end -- optional stop callback
  }, ]]
  
  --ACADEMIA
  {
    name="Academia", -- menu name
    r=255,g=125,b=0, -- color
    max_units=1000,
    units_per_minute=1000,
    x=-1202.96252441406,y=-1566.14086914063,z=4.61040639877319, -- pos
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Força"] = { -- action name
        description="Aumente sua força e stamina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={ -- optional
          ["physical.strength"] = 1 -- "group.aptitude", give 1 exp per unit
        }
      }
    }
  },
  ---------------------------DROGAS -----------------------

  --                         CAMPOS                      --
  --CAMPO DE MACONHA
  {
    name="Campo de Ervas", -- menu name
    permissions = {"harvest.weed"}, -- you can add permissions
    r=0,g=200,b=0, -- color
    max_units=200,
    units_per_minute=5,
    x=1268.6669921875,y=1858.9072265625,z=82.186492919922, -- pos
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- action name
        description="Colher maconha.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["weed"] = 1
        }
      }
    }
  },
  -- CAMPO DE METANFETAMINA    
  {
    name="Campo de Metanfetamina", -- Nome do menu
    permissions = {"harvest.metanfetamina"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=-505.83407592773,y=5156.7890625,z=90.913375854492, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Crystal Melamine.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["crystalmelamine"] = 1
        }
      }
    }
  },  
  -- CAMPO DE HEROINA 
  {
    name="Campo de Heroina", -- Nome do menu
    permissions = {"harvest.heroina"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=2202.748046875,y=5627.4584960938,z=56.154983520508, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Papaver somniferum.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["opio"] = 1
        }
      }
    }
  },   
  -- CAMPO DE CRACK  
  {
    name="Campo de Crack", -- Nome do menu
    permissions = {"harvest.crack"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=-2977.6457519531,y=1607.5068359375,z=21.268037796021, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Crystal de Crack.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["cristal"] = 1
        }
      }
    }
  },  
  -- CAMPO DE LANCA 
  {
    name="Campo de Lanca perfume", -- Nome do menu
    permissions = {"harvest.lanca"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=2821.0607910156,y=-741.66400146484,z=1.9846405982971, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Cloreto de etila.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["cloreto"] = 1
        }
      }
    }
  },   
  -- CAMPO DE COCAINA    
  {
    name="Campo de Cocaina", -- Nome do menu
    permissions = {"harvest.cocaina"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=-463.15362548828,y=1599.1378173828,z=360.22189331055, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher folha de Coca.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["folhadecoca"] = 1
        }
      }
    }
  },
  --                     PROCESSAMENTO                  --

  -- PROCESSADOR DE METANFETAMINA
  {
    name="Processador de Metanfetamina", -- menu name
    permissions = {"process.metanfetamina"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=845.31506347656,y=-291.71832275391,z=73.256881713867, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Metanfetamina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["crystalmelamine"] = 1
	    },
        products={ -- items given per unit
          ["metanfetamina"] = 2
        }
      }
    }
  }, 
  -- PROCESSAMENTO DE HEROINA  
  {
    name="Processador de Heroina", -- menu name
    permissions = {"process.heroina"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1320.4366455078,y=-473.33395385742,z=81.444160461426, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Heroina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["opio"] = 1
	    },
        products={ -- items given per unit
          ["heroina"] = 2
        }
      }
    }
  },
  -- PROCESSADOR DE COCAINA
  {
    name="Processador de cocaina", -- menu name
    permissions = {"process.cocaina"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=997.75207519531,y=-128.54899597168,z=86.507797241211, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Cocaina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["folhadecoca"] = 1
	    },
        products={ -- items given per unit
          ["cocaina"] = 2
        }
      }
    }
  },
  -- PROCESSADOR DE COCAINA
  {
    name="Processador de cocaina2", -- menu name
    permissions = {"process.cocaina2"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1981.7066650391,y=3030.4665527344,z=47.056289672852, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Cocaina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["folhadecoca"] = 1
      },
        products={ -- items given per unit
          ["cocaina"] = 2
        }
      }
    }
  },
    -- PROCESSAMENTO DE CRACK   
  {
    name="Processador de Crack", -- menu name
    permissions = {"process.crack"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1394.6291503906,y=3601.8801269531,z=38.94189453125, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Crack.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["cristal"] = 1
	    },
        products={ -- items given per unit
          ["crack"] = 2
        }
      }
    }
  },
  -- PROCESSADOR DE LANCA
  {
    name="Processador de Lanca Perfume", -- menu name
    permissions = {"process.lanca"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1392.0981445313,y=3605.3110351563,z=38.941932678223,  -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Lanca Perfume.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["cloreto"] = 1
	    },
        products={ -- items given per unit
          ["lanca"] = 2
        }
      }
    }
  },
  -- PROCESSAMENTO DE MACONHA  
  {
    name="Processar Ervas", -- menu name
    permissions = {"process.weed"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=-810.89904785156,y=611.54016113281,z=131.46493530273, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Maconha.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["weed"] = 1
    },
        products={ -- items given per unit
      ["cannabis"] = 2
        }
      }
    }
  },

  ---------------------OUTROS TRABALHOS--------------------

  --                       COLETA                        -- 

  -- CAMPO DE MINERIO
  {
    name="Campo de Minerio", -- Nome do menu
    permissions = {"campodeminerio"}, -- you can add permissions
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=2,
    x=1534.5666503906,y=6327.1376953125,z=24.215845108032, -- Localização    
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher pedras para lavagem.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["Pedra"] = 1
        }
      }
    }
  },
  -- CARRO FORTE
  {
    name="Cofre", -- menu name
	  permissions = {"bankdriver.money"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=1,
    units_per_minute=1,
    x=253.90089416504,y=225.21408081055,z=101.87578582764,
    radius=2, height=1.0, -- area
    recipes = {
      ["Dinheiro do Banco"] = { -- action name
        description="Pegar dinheiro do banco.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		      ["bank_money"] = 500000
		    }, -- items given per unit
        aptitudes={} -- optional
      }
     }
  },
  -- PORTE DE ARMA
  {
    name="Porte de Arma",
    permissions = {"portedearma.civil"}, -- menu name
    r=255,g=125,b=0, -- color
    max_units=1,
    units_per_minute=1,
    x=437.178802490234,y=-994.613525390625,z=30.6895904541016,
    radius=2, height=1.0, -- area
    recipes = {
      ["Porte de Arma"] = { -- action name
       description="comprar porte de arma.", -- action description
        in_money=1500, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
    ["portedearma"] = 1
    }, -- items given per unit
        aptitudes={} -- optional
      }
    }
  },
  -- PROCESSAMENTO DE SUCO
  {
    name="Processar Sucos", -- menu name
    permissions = {"process.suco"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=-1924.3863525391,y=2059.2233886719,z=140.83378601074, -- pos
    radius=5.5, height=1.5, -- area
    recipes = {
   ["Suco de Laranja"] = { -- action name
      description="Processar Laranja.", -- action description
      in_money=100, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["laranja"] = 1
    
     },
       products={ -- items given per unit
     ["orangejuice"] = 2
     }, 
    },
    ["Limonada"] = { -- action name
      description="Processar Limão.", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["limao"] = 5
    
     },
       products={ -- items given per unit
     ["lemonlimonad"] = 3
     },
    },
   },
  },
  -- PROCESSAMENTO DE SEMENTE
  {
    name="Plantação", -- menu name
    permissions = {"process.semente"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=-1766.1224365234,y=1943.3718261719,z=130.55781555176, -- pos
    radius=5.5, height=1.5, -- area
    recipes = {
    ["Plantar Milho"] = { -- action name
      description="Plantar semente de Milho.", -- action description
      in_money=100, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["sementemilho"] = 10
    
     },
       products={ -- items given per unit
     ["milho"] = 1
     }, 
    },
    ["Plantar Melancia"] = { -- action name
     description="Plantar semente de Melancia.", -- action description
      in_money=200, -- money taken per unit
      out_money=0, -- money earned per unit
     reagents={ -- items taken per unit
     ["sementemelancia"] = 5
    
     },
     products={ -- items given per unit
    ["melancia"] = 1
    },
   },
   },
  },
  -- PROCESSAMENTO DE CARNE
  {
    name="Processar Carne", -- menu name
    permissions = {"process.carne"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=994.76928710938,y=-2162.5554199219,z=30.410623550415, -- pos
    radius=5.5, height=1.5, -- area
    recipes = {
    ["Processar Carne in Natura"] = { -- action name
      description="Processar carne in natura.", -- action description
      in_money=100, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["carneinnatura"] = 1
     },
       products={ -- items given per unit
     ["picanha"] = 1
    },
   },
  },
 },
  -- PROCESSAMENTO DE FRANGO
  {
    name="Processar Frango", -- menu name
    permissions = {"process.Frango"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=-84.098526000977,y=6232.283203125,z=31.091291427612, -- pos
    radius=5.5, height=1.5, -- area
    recipes = {
    ["Processar Frango"] = { -- action name
      description="Processar Frango.", -- action description
      in_money=100, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["frango"] = 1
    
     },
       products={ -- items given per unit
     ["ffrango"] = 1
     }, 
    },
   },
  },
  -- TRANSPORTE DE MEDICAMENTOS 1
  {
    name="Farmacia", -- menu name
    permissions = {"process.medical"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=355.43530273438,y=-596.13916015625,z=28.773334503174, -- pos
    radius=5.5, height=1.5, -- area
    recipes = {
    ["Processar clonazepam"] = { -- action name
      description="Processar clonazepam.", -- action description
      in_money=100, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["clonazepam"] = 1
    
     },
       products={ -- items given per unit
     ["rivotril"] = 1
     }, 
    },
   },
  },
  -- TRANSPORTE DE MEDICAMENTOS 1
  {
    name="Farmacia", -- menu name
    permissions = {"process.medical2"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=2, -- unidades que o transformador ganha de volta por minuto
    x=355.43530273438,y=-596.13916015625,z=28.773334503174, -- pos
    radius=5.5, height=1.5, -- area
    recipes = {
    ["Processar Paracetamol"] = { -- action name
      description="Processar paracetamol.", -- action description
      in_money=100, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["paracetamol"] = 1
    
     },
       products={ -- items given per unit
     ["tylenol"] = 1
     }, 
    },
   },
  },
  -- PROCESSADOR DE MINERIO
  {
    name="Lavagem de Minerio", -- menu name
    permissions = {"lavagemdeminerio"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=2, -- unidades que o transformador ganha de volta por minuto
    x=-554.48089599609,y=5324.0986328125,z=73.599700927734, -- pos
    radius=7.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Lavagem de Pedra"] = { -- action name
        description="Produzir minerio...", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["Pedra"] = 1
        },
        products={ -- items given per unit
          ["Minerio"] = 1
        }
      }
    }
  },
  --- PESCADOR ---
  {
    name="Fishing", -- menu name
    permissions = {"process.fish"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=300,
    units_per_minute=1,
    x=743.19586181641,y=3895.3967285156,z=30.5, 
    radius=3, height=1.5, -- area
    recipes = {
      ["Pegando algum Bagre"] = { -- action name
        description="Tentando pegar uns Bagre", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["bagre"] = 1
        }
      },
      ["Pegando alguns Pirarucu"] = { -- action name
        description="Pegar Pirarucu", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["pirarucu"] = 1
        }
      }
    }
  },
  --- CONTRABANDISTA DE ARMA ---
  {
    name="Weapons Smuggler", -- menu name
    permissions = {"process.smuggler"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=100,
    units_per_minute=0.3,
    x=-343.28500366211,y=6098.6586914063,z=31.327739715576,
    radius=3, height=1.5, -- area
    recipes = {
      ["Pegar AK47"] = { -- action name
        description="Pegar AK47", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["AK47"] = 1
        }
      },
      ["Pegar M4A1"] = { -- action name
        description="Pegar M4A1", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["M4A1"] = 1
        }
      }
    }
  },
  {
    name="Hacker", -- menu name
	permissions = {"hacker.credit_cards"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=707.357238769531,y=-966.98876953125,z=30.4128551483154,
    radius=2, height=1.0, -- area
    recipes = {
      ["hacking"] = { -- action name
        description="Hackeando alguns cartoes.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["credit"] = 1,
		["dirty_money"] = 1
		}, -- items given per unit
        aptitudes={ -- optional
          ["hacker.hacking"] = 0.2 -- "group.aptitude", give 1 exp per unit
        }
      }
    }
  },

  

}



-- define multiple static transformers with postions list
local weedplants = {
  {1873.36901855469,3658.46215820313,33.8029747009277},
  {1856.33776855469,3635.12109375,34.1897926330566},
  {1830.75390625,3621.44140625,33.8487205505371},
  {1784.70251464844,3579.93798828125,34.7956123352051},
  {182.644317626953,3315.75537109375,41.4806938171387},
  {1764.65661621094,3642.916015625,34.5866050720215},
  {1512.91027832031,1673.76025390625,111.531875610352}
 }
 for k,v in pairs(weedplants) do
  local plant = {
    name="Planta de Maconha", -- menu name
    --permissions = {"harvest.weed"}, -- you can add permissions
    r=0,g=200,b=0, -- color
    max_units=1,
    units_per_minute=1,
    x=v[1],y=v[2],z=v[3], -- pos
    radius=5.0, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- action name
        description="Colher maconha.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["weed"] = 1
        }
      }
    }
  }
  table.insert(cfg.item_transformers, plant)
end

local warehouses = {
  {-1111.2841796875,4937.4052734375,218.386306762695},
  {1539.01794433594,1704.29174804688,109.659622192383},
  {981.412841796875,-1805.70349121094,35.4845695495605}
 }
 for k,v in pairs(warehouses) do
  local warehouse = {
    name="Oficina de armas", -- menu name
    permissions = {"build.gun"}, -- you can add permissions
    r=0,g=200,b=0, -- color
    max_units=10,
    units_per_minute=1,
    x=v[1],y=v[2],z=v[3], -- pos
    radius=5.0, height=1.5, -- area
    recipes = {
      ["Build Pistol"] = { -- action name
        description="Construir pistola.", -- action description
        in_money=1500, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
		  ["pistol_parts"] = 1
		}, -- items taken per unit
        products={ -- items given per unit
          ["wbody|WEAPON_PISTOL"] = 1
        }
      },
      ["Build Shotgun"] = { -- action name
        description="Construir shotgun.", -- action description
        in_money=3000, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
		  ["shotgun_parts"] = 1
		}, -- items taken per unit
        products={ -- items given per unit
          ["wbody|WEAPON_PUMPSHOTGUN"] = 1
        }
      },
      ["Build SMG"] = { -- action name
        description="Construir submachinegun.", -- action description
        in_money=5000, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
		  ["smg_parts"] = 1
		}, -- items taken per unit
        products={ -- items given per unit
          ["wbody|WEAPON_SMG"] = 1
        }
      }
    }
  }
  table.insert(cfg.item_transformers, warehouse)
end


-- define transformers randomly placed on the map
cfg.hidden_transformers = {
  ["weed plant"] = {
    def = {
      name="Weed Plant", -- menu name
      r=0,g=200,b=0, -- color
      max_units=50,
      units_per_minute=0,
      x=0,y=0,z=0, -- pos
      radius=0, height=0, -- area
      recipes = {
      }
    },
    positions = weedplants
  },
  ["gun warehouse"] = {
    def = {
      name="Gun Warehouse", -- menu name
      r=0,g=200,b=0, -- color
      max_units=50,
      units_per_minute=0,
      x=0,y=0,z=0, -- pos
      radius=0, height=0, -- area
      recipes = {
      }
    },
    positions = warehouses
  }
}

-- time in minutes before hidden transformers are relocated (min is 5 minutes)
cfg.hidden_transformer_duration = 30-- 12 hours -- 5*24*60 -- 5 days

-- configure the information reseller (can sell hidden transformers positions)
cfg.informer = { 
  infos = {
    ["weed plant"] = 10000,
    ["gun warehouse"] = 25000
  },
  positions = {
    {1821.12390136719,3685.9736328125,34.2769317626953},
    {1804.2958984375,3684.12280273438,34.217945098877}
  },
  interval = 30, -- interval in minutes for the reseller respawn
  duration = 10, -- duration in minutes of the spawned reseller
  blipid = 0,
  blipcolor = 0
}
return cfg
