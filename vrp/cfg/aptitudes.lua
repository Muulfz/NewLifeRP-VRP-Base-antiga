
local cfg = {}

-- exp notes:
-- levels are defined by the amount of xp
-- with a step of 5: 5|15|30|50|75 (by default)
-- total exp for a specific level, exp = step*lvl*(lvl+1)/2
-- level for a specific exp amount, lvl = (sqrt(1+8*exp/step)-1)/2

-- define groups of aptitudes
--- _title: title of the group
--- map of aptitude => {title,init_exp,max_exp}
---- max_exp: -1 for infinite exp
cfg.gaptitudes = {
  ["physical"] = {
    _title = "Fisico",
    ["strength"] = {"Força", 30, 550} -- required, level 3 to 6 (by default, can carry 10kg per level)
  },
  ["science"] = {
    _title = "Ciencia",
    ["produtos químicos"] = {"Estudo de produtos químicos", 0, -1}, -- example
    ["matematica"] = {"Estudo de Matematica", 0, -1} -- example
  },
  ["laboratory"] = {
    _title = "Laboratorio de Drogas",
	["cocaine"] = {"Processe sua cocaína (necessita benzoilmetilecgonina, encontrado na plantação).", 0, -1},
	["weed"] = {"Processe sua erva (necessita sementes, encontradas na plantação).", 0, -1},
	["lsd"] = {"Processe sua LSD (necessita Arnês, encontrada na plantação).", 0, -1}
  },
  ["hacker"] = {
    _title = "Estudo Hacker",
	["logic"] = {"Estudo de Logica.", 0, -1},
	["c++"] = {"Estudo de c++ .", 0, -1},
	["lua"] = {"Estudo de Lua.", 0, -1},
	["hacking"] = {"Estudo Hacker.", 0, -1}
  }
}

return cfg
