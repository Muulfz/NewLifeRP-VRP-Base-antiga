
local cfg = {}

-- example of study transformer
local itemtr_study = {
  name="Estante de Livros", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20,
  units_per_minute=10,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Livros de Produtos Quimicos"] = { -- action name
      description="Leia um livro de produtos Quimicos", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={}, -- items taken per unit
      products={}, -- items given per unit
      aptitudes={ -- optional
        ["science.chemicals"] = 1 -- "group.aptitude", give 1 exp per unit
      }
    },
    ["Livros de Matemática"] = { -- action name
      description="Leia um Livro de Matemática", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={}, -- items taken per unit
      products={}, -- items given per unit
      aptitudes={ -- optional
        ["science.mathematics"] = 1 -- "group.aptitude", give 1 exp per unit
      }
    }
  }
}

----------------------------------------------------------------------------------------- laboratory
local itemtr_laboratory = {
  name="Mesa de Processamento", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20,
  units_per_minute=20,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
	  ["folhadecoca"] = 1
	  
	  },
      products={ -- items given per unit
	  ["cocaina"] = 1
	  }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
		["science.chemicals"] = 10
	  }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
	  ["crystalmelamine"] = 1
	  
	  },
      products={ -- items given per unit
	  ["metanfetamina"] = 1
	  }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
		["science.chemicals"] = 10
	  }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
	  ["opio"] = 1
	  
	  },
      products={ -- items given per unit
	  ["heroina"] = 1
	  }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
		["science.chemicals"] = 10
	  }
    },	
	["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
	  ["weed"] = 1
	  },
      products={ -- items given per unit
	  ["cannabis"] = 1
	  }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
		["science.chemicals"] = 10
	  }
    },
  }
}

local itemtr_vip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20,
  units_per_minute=20,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_puteirovip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=200000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_aviariovip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=200000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Frangos"] = { -- action name
      description="Processar Frango", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["frango"] = 1
    
    },
      products={ -- items given per unit
    ["ffrango"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_acoguevip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=200000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Carne"] = { -- action name
      description="Processar Carne", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["carneinnatura"] = 1
    
    },
      products={ -- items given per unit
    ["picanha"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_tekilavip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Carne"] = { -- action name
      description="Processar Carne", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["carneinnatura"] = 1
    
    },
      products={ -- items given per unit
    ["picanha"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_thelostvip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Carne"] = { -- action name
      description="Processar Carne", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["carneinnatura"] = 1
    
    },
      products={ -- items given per unit
    ["picanha"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_caveirasvip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Carne"] = { -- action name
      description="Processar Carne", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["carneinnatura"] = 1
    
    },
      products={ -- items given per unit
    ["picanha"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_fdcvip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Carne"] = { -- action name
      description="Processar Carne", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["carneinnatura"] = 1
    
    },
      products={ -- items given per unit
    ["picanha"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_barravip = {
  name="Mesa de Processamento Vip", -- menu name
  r=0,g=255,b=0, -- color
  max_units=2000000,
  units_per_minute=1000,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["Dinheiro de Roubo"] = { -- action name
      description="Distruibuir dinheiro proviniente de roubo", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["dinheiro_roubo"] = 1000
    
    },
      products={ -- items given per unit
    ["money"] = 1000
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
    ["Cocaina"] = { -- action name
      description="Processar Cocaina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
     ["folhadecoca"] = 1
    
    },
      products={ -- items given per unit
    ["cocaina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Metanfetamina"] = { -- action name
      description="Processar Metanfetamina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["crystalmelamine"] = 1
    
    },
      products={ -- items given per unit
    ["metanfetamina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
    ["Heroina"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["opio"] = 1
    
    },
      products={ -- items given per unit
    ["heroina"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },  
    ["Crack"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cristal"] = 1
    
    },
      products={ -- items given per unit
    ["crack"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Lanca Perfume"] = { -- action name
      description="Processar Heroina", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["cloreto"] = 1
    
    },
      products={ -- items given per unit
    ["lanca"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },   
    ["Carne"] = { -- action name
      description="Processar Carne", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["carneinnatura"] = 1
    
    },
      products={ -- items given per unit
    ["picanha"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.cocaine"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    }, 
  ["Maconha"] = { -- action name
      description="Processar Maconha", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={ -- items taken per unit
    ["weed"] = 1
    },
      products={ -- items given per unit
    ["cannabis"] = 2
    }, 
      aptitudes={ -- optional
        ["laboratory.weed"] = 5, -- "group.aptitude", give 1 exp per unit
    ["science.chemicals"] = 10
    }
    },
  }
}

local itemtr_hacker = {
  name="hacker", -- menu name
  r=0,g=255,b=0, -- color
  max_units=20,
  units_per_minute=10,
  x=0,y=0,z=0, -- pos (doesn't matter as home component)
  radius=1.1, height=1.5, -- area
  recipes = {
    ["logic pdf"] = { -- action name
     description="Leia o pdf sobre Logica", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={}, -- items taken per unit
      products={}, -- items given per unit
      aptitudes={ -- optional
        ["hacker.logic"] = 10 -- "group.aptitude", give 1 exp per unit
      }
    },
    ["c++ pdf"] = { -- action name
      description="Leia o pdf de C++ ", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={}, -- items taken per unit
      products={}, -- items given per unit
      aptitudes={ -- optional
        ["hacker.c++"] = 1 -- "group.aptitude", give 1 exp per unit
      }
    },
	  ["lua pdf"] = { -- action name
      description="Leia o pdf de lua", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={}, -- items taken per unit
      products={}, -- items given per unit
      aptitudes={ -- optional
        ["hacker.lua"] = 1 -- "group.aptitude", give 1 exp per unit
      }
    },
	["hacking"] = { -- action name
      description="Hackear informacoes de cartao de credito", -- action description
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={}, -- items taken per unit
      products={
	  ["dirty_money"] = 50
	  }, -- items given per unit
      aptitudes={ -- optional
        ["hacker.lua"] = 1, -- "group.aptitude", give 1 exp per unit
        ["hacker.c++"] = 1, -- "group.aptitude", give 1 exp per unit
        ["hacker.logic"] = 1, -- "group.aptitude", give 1 exp per unit
        ["hacker.hacking"] = 1 -- "group.aptitude", give 1 exp per unit
      }
    },
  }
}
-- default flats positions from https://github.com/Nadochima/HomeGTAV/blob/master/List

-- define the home slots (each entry coordinate should be unique for ALL types)
-- each slots is a list of home components
--- {component,x,y,z} (optional _config)
--- the entry component is required
cfg.slot_types = {
  ["basic_flat"] = {
    {
      {"entry",-782.171,324.589,223.258},
      {"chest",-773.718078613281,325.144409179688,223.266357421875, _config = {weight=200}},
      {"wardrobe",-760.885437011719,325.457153320313,217.061080932617},
      {"gametable",-755.40185546875,318.263214111328,221.875823974609},
	  {"itemtr", _config = itemtr_laboratory, -772.294372558594,328.913177490234,223.261581420898},
      {"itemtr", _config = itemtr_study, -758.670104980469,315.048156738281,221.854904174805}
    },
    {
      {"entry",-774.171,333.589,207.621},
      {"chest",-782.118591308594,332.147399902344,207.629608154297, _config = {weight=200}},
      {"wardrobe",-795.118469238281,331.631256103516,201.42431640625},
      {"gametable",-800.763427734375,338.574951171875,206.239105224609},
	  {"itemtr", _config = itemtr_laboratory, -783.788635253906,328.553985595703,207.625396728516},
      {"itemtr", _config = itemtr_study, -797.283447265625,342.134338378906,206.21842956543}
    },
    {
      {"entry",-774.171,333.589,159.998},
      {"chest",-782.66015625,332.523284912109,160.010223388672, _config = {weight=200}},
      {"wardrobe",-795.550964355469,332.229614257813,153.804931640625},
      {"gametable",-801.228942871094,339.106231689453,158.619750976563},
	  {"itemtr", _config = itemtr_laboratory, -784.178588867188,328.916839599609,160.006057739258},
      {"itemtr", _config = itemtr_study, -797.896728515625,342.543273925781,158.599044799805}
    },
    {
      {"entry",-596.689,59.139,108.030},
      {"chest",-604.427001953125,57.0807762145996,108.035743713379, _config = {weight=200}},
      {"wardrobe",-617.180358886719,56.9536590576172,101.830520629883},
      {"gametable",-623.078796386719,63.688045501709,106.645317077637},
	  {"itemtr", _config = itemtr_laboratory, -605.9560546875,53.3968696594238,108.031196594238},
      {"itemtr", _config = itemtr_study, -619.724853515625,67.1367950439453,106.624366760254}
    },
    {
      {"entry",-1451.557,-523.546,69.556},
      {"chest",-1457.28601074219,-529.699523925781,69.565315246582, _config = {weight=200}},
      {"wardrobe",-1467.07995605469,-536.98583984375,63.3601188659668},
      {"gametable",-1476.12658691406,-534.873474121094,68.1748962402344},
	  {"itemtr", _config = itemtr_laboratory, -1456.35876464844,-533.55029296875,69.5607528686523},
      {"itemtr", _config = itemtr_study, -1475.36840820313,-530.117126464844,68.1540756225586}
    },
    {
      {"entry",-1452.185,-522.640,56.929},
      {"chest",-1457.3740234375,-529.737854003906,56.9376449584961, _config = {weight=200}},
      {"wardrobe",-1467.7158203125,-537.308349609375,50.732494354248},
      {"gametable",-1476.12670898438,-534.895751953125,55.547306060791},
	  {"itemtr", _config = itemtr_laboratory, -1456.32409667969,-533.43701171875,56.9333839416504},
      {"itemtr", _config = itemtr_study, -1475.39453125,-530.135192871094,55.5264129638672}
    },
    {
      {"entry",-907.900,-370.608,109.440},
      {"chest",-914.79296875,-375.26416015625,109.448974609375, _config = {weight=200}},
      {"wardrobe",-926.047912597656,-381.470153808594,103.243774414063},
      {"gametable",-934.216979980469,-378.082336425781,108.05859375},
	  {"itemtr", _config = itemtr_laboratory, -914.430541992188,-379.17333984375,109.444869995117},
      {"itemtr", _config = itemtr_study, -932.810302734375,-373.413330078125,108.03776550293}
    },
    {
      {"entry",-921.124,-381.099,85.480},
      {"chest",-915.268737792969,-375.818084716797,85.4891815185547, _config = {weight=200}},
      {"wardrobe",-904.0673828125,-369.910552978516,79.2839508056641},
      {"gametable",-895.890075683594,-373.167846679688,84.0987701416016},
	  {"itemtr", _config = itemtr_laboratory, -915.567504882813,-371.905731201172,85.4850234985352},
      {"itemtr", _config = itemtr_study, -897.224792480469,-377.828887939453,84.0779266357422},
    },
    {
      {"entry",-464.453,-708.617,77.086},
      {"chest",-466.566558837891,-700.528991699219,77.0956268310547, _config = {weight=200}},
      {"wardrobe",-466.892852783203,-687.88720703125,70.8903503417969},
      {"gametable",-459.870513916016,-682.054565429688,75.7051773071289},
	  {"itemtr", _config = itemtr_laboratory, -470.110504150391,-699.137634277344,77.0915145874023},
      {"itemtr", _config = itemtr_study, -456.510467529297,-685.274841308594,75.6842498779297},
    },
    {
      {"entry",-470.647,-689.459,53.402},
      {"chest",-469.077453613281,-697.664672851563,53.4144515991211, _config = {weight=200}},
      {"wardrobe",-468.939910888672,-710.398986816406,47.2093048095703},
      {"gametable",-475.543884277344,-716.226867675781,52.0241050720215},
	  {"itemtr", _config = itemtr_laboratory, -465.526031494141,-699.133666992188,53.4103393554688},
      {"itemtr", _config = itemtr_study, -479.113037109375,-712.874938964844,52.0032043457031},
    }
  },
  ["other_flat"] = {
    {
      {"entry",-784.363,323.792,211.996},
      {"chest",-766.744384765625,328.375,211.396545410156, _config = {weight=500}},
      {"wardrobe",-793.502136230469,326.861846923828,210.796630859375},
      {"gametable",-781.973083496094,338.788482666016,211.231979370117},
	  {"itemtr", _config = itemtr_laboratory, -763.146362304688,326.994598388672,211.396560668945},
      {"itemtr", _config = itemtr_study, -778.560241699219,333.439453125,211.197128295898}
    },
    {
      {"entry",-603.997,58.954,98.200},
      {"chest",-621.323669433594,54.2074241638184,97.5995330810547, _config = {weight=500}},
      {"wardrobe",-594.668823242188,55.5750961303711,96.9996185302734},
      {"gametable",-606.140441894531,43.8849754333496,97.4350128173828},
	  {"itemtr", _config = itemtr_laboratory, -624.831909179688,55.5965461730957,97.5995635986328},
      {"itemtr", _config = itemtr_study, -608.968322753906,49.1769981384277,97.4001312255859}
    },
    {
      {"entry",-1453.013,-539.629,74.044},
      {"chest",-1466.57763671875,-528.339294433594,73.4436492919922, _config = {weight=500}},
      {"wardrobe",-1449.85034179688,-549.231323242188,72.8437194824219},
      {"gametable",-1466.37182617188,-546.663757324219,73.279052734375},
	  {"itemtr", _config = itemtr_laboratory, -1467.62377929688,-524.550842285156,73.4436492919922},
      {"itemtr", _config = itemtr_study, -1463.84411621094,-541.1962890625,73.2442169189453}
    },
    {
      {"entry",-912.547,-364.706,114.274},
      {"chest",-926.693176269531,-377.596130371094,113.674102783203, _config = {weight=500}},
      {"wardrobe",-903.66650390625,-364.023223876953,113.074157714844},
      {"gametable",-908.407165527344,-379.825714111328,113.509590148926},
	  {"itemtr", _config = itemtr_laboratory, -930.571899414063,-378.051239013672,113.674072265625},
      {"itemtr", _config = itemtr_study, -913.553588867188,-376.692016601563,113.474617004395}
    }
  },
  ["fazenda_flat"] = {
    {
      {"entry",1396.9913330078,1141.8450927734,114.33367919922},
      {"chest",1398.0871582031,1164.1635742188,114.33367919922, _config = {weight=1500}},
      {"wardrobe",1400.2038574219,1159.5600585938,114.33355712891},
      {"gametable",1397.1551513672,1132.1381835938,114.33355712891},
      {"itemtr", _config = itemtr_laboratory, 1393.8511962891,1145.1695556641,114.33367919922}
    }
  },
  ["vip1_flat"] = {
    {
      {"entry",-174.10354614258,497.32022094727,137.66696166992},
      {"chest",-174.37693786621,493.63809204102,130.04371643066, _config = {weight=800}},
      {"wardrobe",-167.42747497559,487.7958984375,133.84381103516},
      {"gametable",-165.07147216797,483.78701782227,137.26531982422},
      {"itemtr", _config = itemtr_vip, -175.00372314453,489.96774291992,130.04371643066}
    }
  },
  ["vip2_flat"] = {
    {
      {"entry",341.64733886719,437.40972900391,149.39405822754},
      {"chest",338.08242797852,436.81604003906,141.77076721191, _config = {weight=800}},
      {"wardrobe",334.31918334961,428.46383666992,145.57081604004},
      {"gametable",330.9817199707,425.03125,148.99258422852},
      {"itemtr", _config = itemtr_vip, 334.33459472656,436.2243347168,141.77076721191}
    }
  },
  ["vip3_flat"] = {
    {
      {"entry",373.74584960938,423.25018310547,145.90786743164},
      {"chest",376.92370605469,429.30517578125,138.30017089844, _config = {weight=800}},
      {"wardrobe",374.54263305664,411.5041809082,142.10012817383},
      {"gametable",373.52014160156,404.83322143555,145.52745056152},
      {"itemtr", _config = itemtr_vip, 379.07656860352,432.37295532227,138.30017089844}
    }
  },
  ["vip4_flat"] = {
    {
      {"entry",-682.23968505859,592.53051757813,145.39295959473},
      {"chest",-680.38427734375,588.95416259766,137.76976013184, _config = {weight=800}},
      {"wardrobe",-671.38696289063,587.29095458984,141.56988525391},
      {"gametable",-667.27911376953,585.09533691406,144.99156188965},
      {"itemtr", _config = itemtr_vip, -679.20172119141,585.43780517578,137.76976013184}
    }
  },
  ["vip5_flat"] = {
    {
      {"entry",-1289.6420898438,449.28283691406,97.902503967285},
      {"chest",-1287.9265136719,455.72882080078,90.294708251953, _config = {weight=800}},
      {"wardrobe",-1285.9825439453,438.18774414063,94.094734191895},
      {"gametable",-1285.4017333984,431.41772460938,97.522132873535},
      {"itemtr", _config = itemtr_vip, -1286.7156982422,459.36468505859,90.294708251953}
    }
  },
  ["vip7_flat"] = {
    {
      {"entry",3261.9548339844,-124.22022247314,15.73699092865},
      {"chest",3269.3723144531,-137.58309936523,20.621995925903, _config = {weight=1500}},
      {"wardrobe",3268.2819824219,-129.72662353516,20.5481796264654},
      {"gametable",3256.2277832031,-135.10739135742,17.1393947601323},
      {"itemtr", _config = itemtr_vip, 3248.8449707031,-130.89868164063,20.536575317383}
    }
  },
  ["vip8_flat"] = {
    {
      {"entry",725.89916992188,-924.60803222656,26.862314224243},
      {"chest",729.29162597656,-929.16802978516,25.438949584961, _config = {weight=1500}},
      {"wardrobe",725.83038330078,-930.33148193359,28.882625579834},
      {"gametable",750.11895751953,-925.58703613281,25.478288650513},
      {"itemtr", _config = itemtr_vip, 724.36193847656,-920.10919189453,25.438932418823}
    }
  }, 
  ["vip9_flat"] = {
    {
      {"entry",-811.37219238281,181.27052307129,76.740791320801},
      {"chest",-811.37219238281,181.27052307129,76.740791320801, _config = {weight=1500}},
      {"wardrobe",-812.08831787109,175.04960632324,76.74535369873},
      {"gametable",-803.66455078125,174.78802490234,72.84464263916},
      {"itemtr", _config = itemtr_vip, -806.78192138672,167.21447753906,76.740791320801}
    }
  },
  ["vip10_flat"] = {
    {
      {"entry",2848.6594238281,-740.82336425781,6.2554683685303},
      {"chest",2836.6032714844,-760.99938964844,6.3265647888184, _config = {weight=1500}},
      {"wardrobe",2841.71484375,-757.75604248047,6.3465633392334},
      {"gametable",2854.6450195313,-754.58361816406,12.569466590881},
      {"itemtr", _config = itemtr_vip, 2855.0397949219,-757.43499755859,2.8799314498901}
    }
  },
  ["vip11_flat"] = {
    {
      {"entry",-111.2639465332,999.61486816406,235.75679016113},
      {"chest",-795.00433349609,326.32498168945,217.03819274902, _config = {weight=1500}},
      {"wardrobe",-796.93218994141,329.65960693359,220.44085693359},
      {"gametable",-783.61053466797,339.65838623047,216.85133361816},
      {"itemtr", _config = itemtr_vip, -799.45574951172,327.71209716797,217.03820800781}
    }
  },
  ["Favela_gro1"] = {
    {
      {"entry",28.466054916382,-1852.6987304688,30.569931030273},
      {"chest",35.540592193604,-1848.6453857422,30.569929122925, _config = {weight=150}},
    }
  }, 
  ["Favela_gro2"] = {
    {
      {"entry",51.529197692871,-1872.0551757813,29.46813583374},
      {"chest",58.593059539795,-1866.9718017578,29.46813583374, _config = {weight=150}},
    }
  },
  ["Favela_gro3"] = {
    {
      {"entry",103.76905822754,-1916.77734375,28.044139862061},
      {"chest",110.13542175293,-1911.5026855469,28.044160842896, _config = {weight=150}},
    }
  },
  ["Favela_gro4"] = {
    {
      {"entry",87.97802734375,-1958.7456054688,27.983936309814},
      {"chest",80.909553527832,-1964.1209716797,27.983936309814, _config = {weight=150}},
    }
  },
  ["Favela_gro5"] = {
    {
      {"entry",6.6238355636597,-1891.9814453125,26.599792480469},
      {"chest",11.350119590759,-1899.390625,26.418476104736, _config = {weight=150}},
    }
  },
  ["Favela_gro6"] = {
    {
      {"entry",485.5051574707,-1704.7720947266,36.510669708252},
      {"chest",479.35113525391,-1710.0310058594,36.510669708252, _config = {weight=150}},
    }
  },
  ["Favela_gro7"] = {
    {
      {"entry",458.85461425781,-1716.158203125,32.805988311768},
      {"chest",453.47247314453,-1721.5776367188,32.805946350098, _config = {weight=150}},
    }
  },
  ["Favela_gro8"] = {
    {
      {"entry",385.50881958008,-1843.8454589844,33.6784324646},
      {"chest",392.07379150391,-1839.5841064453,33.6784324646, _config = {weight=150}},
    }
  },
  ["Favela_gro9"] = {
    {
      {"entry",349.96740722656,-1880.4016113281,32.132080078125},
      {"chest",356.24041748047,-1875.9708251953,32.132080078125, _config = {weight=150}},
    }
  },
  ["Favela_gro10"] = {
    {
      {"entry",221.72969055176,-1881.8234863281,29.194229125977},
      {"chest",216.60447692871,-1889.0323486328,29.194229125977, _config = {weight=150}},
    }
  },
  ["Favela_heli1"] = {
    {
      {"entry",1294.4146728516,-412.94750976563,75.93888092041},
      {"chest",1296.6885986328,-420.78128051758,75.93888092041, _config = {weight=150}},
    }
  },
  ["Favela_heli2"] = {
    {
      {"entry",1306.4036865234,-418.17929077148,76.438896179199},
      {"chest",1312.2458496094,-424.30938720703,76.438896179199, _config = {weight=150}},
    }
  },
  ["Favela_heli3"] = {
    {
      {"entry",1296.2978515625,-497.61560058594,76.925880432129},
      {"chest",1291.5552978516,-504.33630371094,76.925880432129, _config = {weight=150}},
    }
  },
  ["Favela_heli4"] = {
    {
      {"entry",1340.4464111328,-546.37078857422,80.748329162598},
      {"chest",1347.3751220703,-551.41595458984,80.748329162598, _config = {weight=150}},
    }
  },
  ["Favela_heli5"] = {
    {
      {"entry",1338.4342041016,-580.34161376953,81.266502380371},
      {"chest",1332.7232666016,-587.08441162109,81.266502380371, _config = {weight=150}},
    }
  },
  ["Favela_heli6"] = {
    {
      {"entry",1338.5236816406,-579.91534423828,81.266532897949},
      {"chest",1333.3343505859,-586.71160888672,81.266532897949, _config = {weight=150}},
    }
  },
  ["Favela_heli7"] = {
    {
      {"entry",11313.8934326172,-577.0634765625,79.793510437012},
      {"chest",1309.7855224609,-584.00463867188,79.793510437012, _config = {weight=150}},
    }
  },
  ["Favela_heli8"] = {
    {
      {"entry",1347.2987060547,-470.66271972656,80.074836730957},
      {"chest",1356.3824462891,-470.95159912109,80.074836730957, _config = {weight=150}},
    }
  },
  ["Favela_heli9"] = {
    {
      {"entry",1338.533203125,-493.60903930664,78.959289550781},
      {"chest",1331.1165771484,-499.28887939453,78.95938873291, _config = {weight=150}},
    }
  },
  ["Favela_heli10"] = {
    {
      {"entry",1376.8695068359,-652.02069091797,80.251068115234},
      {"chest",1374.2197265625,-660.68646240234,80.251068115234, _config = {weight=150}},
    }
  },
  ["Favela_para1"] = {
    {
      {"entry",-763.30114746094,560.7109375,128.05993652344},
      {"chest",-756.724609375,555.357421875,128.05989074707, _config = {weight=150}},
    }
  },
  ["Favela_para2"] = {
    {
      {"entry",-745.84466552734,557.45977783203,131.59577941895},
      {"chest",-751.53503417969,562.87506103516,131.59582519531, _config = {weight=150}},
    }
  },
  ["Favela_para3"] = {
    {
      {"entry",-755.18005371094,540.06451416016,121.34086608887},
      {"chest",-748.56988525391,536.20135498047,121.08424377441, _config = {weight=150}},
    }
  },
  ["Favela_para4"] = {
    {
      {"entry",-802.63024902344,630.61907958984,134.91125488281},
      {"chest",-800.41790771484,623.30120849609,134.91128540039, _config = {weight=150}},
    }
  },
  ["Favela_para5"] = {
    {
      {"entry",-964.06634521484,672.27679443359,172.53170776367},
      {"chest",-965.60784912109,663.84161376953,172.53173828125, _config = {weight=150}},
    }
  },
  ["Favela_para6"] = {
    {
      {"entry",-973.69525146484,668.14202880859,169.28192138672},
      {"chest",-981.72680664063,671.31781005859,169.281921386726, _config = {weight=150}},
    }
  },
  ["Favela_para7"] = {
    {
      {"entry",-1021.0494384766,647.74114990234,148.03953552246},
      {"chest",-1017.6829223633,655.57678222656,148.03953552246, _config = {weight=150}},
    }
  },
  ["Favela_para8"] = {
    {
      {"entry",-854.62097167969,612.70983886719,115.1950302124},
      {"chest",-862.88006591797,613.41314697266,115.19504547119, _config = {weight=150}},
    }
  },
  ["Favela_para9"] = {
    {
      {"entry",-792.91784667969,500.48748779297,105.94251251221},
      {"chest",-801.21716308594,498.91314697266,105.94251251221, _config = {weight=150}},
    }
  },
  ["Favela_para10"] = {
    {
      {"entry",-787.92523193359,509.44039916992,109.66580963135},
      {"chest",-787.68206787109,501.869140625,109.58535766602, _config = {weight=150}},
    }
  },
  ["Favela_roci1"] = {
    {
      {"entry",811.54382324219,-225.09167480469,69.842910766602},
      {"chest",808.27435302734,-228.59257507324,69.842712402344, _config = {weight=150}},
    }
  },
  ["Favela_roci2"] = {
    {
      {"entry",800.33251953125,-267.12225341797,69.939918518066},
      {"chest",796.50128173828,-263.93029785156,69.939674377441, _config = {weight=150}},
    }
  },
  ["Favela_roci3"] = {
    {
      {"entry",771.72467041016,-181.61036682129,77.947608947754},
      {"chest",768.35546875,-184.72625732422,77.947380065918, _config = {weight=150}},
    }
  },
  ["Favela_roci4"] = {
    {
      {"entry",758.94506835938,-203.20767211914,70.343246459961},
      {"chest",762.11755371094,-199.74465942383,70.342933654785, _config = {weight=150}},
    }
  },
  ["Favela_roci5"] = {
    {
      {"entry",662.45550537109,-303.99551391602,46.127201080322},
      {"chest",658.77453613281,-301.01263427734,46.126956939697, _config = {weight=150}},
    }
  },
  ["Favela_roci6"] = {
    {
      {"entry",663.46350097656,-345.60754394531,38.873859405518},
      {"chest",665.16296386719,-349.68942260742,38.873603820801, _config = {weight=150}},
    }
  },
  ["Favela_roci7"] = {
    {
      {"entry",854.32208251953,-281.10534667969,69.461982727051},
      {"chest",849.72198486328,-279.63134765625,69.461715698242, _config = {weight=150}},
    }
  },
  ["Favela_roci8"] = {
    {
      {"entry",728.75073242188,-244.35256958008,70.046195983887},
      {"chest",732.04272460938,-240.61143493652,70.045974731445, _config = {weight=150}},
    }
  },
  ["thelost_flat"] = {
    {
      {"entry",982.32745361328,-103.38913726807,74.848731994629},
      {"chest",977.15948486328,-104.19081115723,74.845184326172, _config = {weight=800}},
      {"wardrobe",986.90692138672,-92.819602966309,74.845893859863},
      {"gametable",978.79779052734,-94.306663513184,74.868118286133},
      {"itemtr", _config = itemtr_thelostvip, 985.28900146484,-144.65533447266,74.271514892578}
    }
  },
  ["puteiro_flat"] = {
    {
      {"entry",678298950195,-1285.9233398438,29.26876449585},
      {"chest",93.030494689941,-1291.7426757813,29.26876449585, _config = {weight=800}},
      {"wardrobe",104.97268676758,-1303.4431152344,28.768798828125},
      {"gametable",120.11117553711,-1297.2637939453,29.269529342651},
      {"itemtr", _config = itemtr_puteirovip, 112.87266540527,-1305.6412353516,29.269523620605}
    }
  },
  ["tekila_flat"] = {
    {
      {"entry",-561.93707275391,291.85696411133,87.576362609863},
      {"chest",-561.66082763672,289.34268188477,82.176383972168, _config = {weight=800}},
      {"wardrobe",-568.30352783203,280.14828491211,82.975662231445},
      {"gametable",-573.88470458984,287.29809570313,79.17667388916},
      {"itemtr", _config = itemtr_tekilavip, -575.99694824219,291.26397705078,79.176681518555}
    }
  },
  ["aviario_flat"] = {
    {
      {"entry",-72.89241027832,6261.8002929688,31.090183258057},
      {"chest",-68.446426391602,6256.0405273438,31.09016418457, _config = {weight=800}},
      {"wardrobe",-568.30352783203,280.14828491211,82.975662231445},
      {"gametable",-86.942314147949,6237.2485351563,31.090337753296},
      {"itemtr", _config = itemtr_aviariovip, -558.85168457031,289.78659057617,85.37646484375}
    }
  },
  ["motoclube_flat"] = {
    {
      {"entry",1991.0048828125,3054.2280273438,47.214920043945},
      {"chest",1985.3461914063,3048.91796875,47.215034484863, _config = {weight=800}},
      {"wardrobe",1994.7734375,3046.4194335938,47.215167999268},
      {"gametable",1991.0869140625,3049.5632324219,47.215167999268},
    }
  },
  ["motoclubecaveiras_flat"] = {
    {
      {"entry",1020.1603393555,-2305.2370605469,30.121309280396},
      {"chest",1006.7373657227,-2311.7683105469,30.600330352783, _config = {weight=800}},
      {"wardrobe",1003.0741577148,-2312.6274414063,30.600286483765},
      {"gametable",1018.3195800781,-2311.7001953125,30.600328445435},
      {"itemtr", _config = itemtr_caveirasvip, 1003.9207763672,-2310.0900878906,30.600328445435}
    }
  },
  ["filhodacadelaria_flat"] = {
    {
      {"entry",164.77127075195,3164.66796875,43.404491424561},
      {"chest",161.8591003418,3161.4797363281,43.404476165771, _config = {weight=800}},
      {"wardrobe",164.79595947266,3154.2897949219,43.404476165771},
      {"gametable",171.34407043457,3159.048828125,43.404476165771},
      {"itemtr", _config = itemtr_fdcvip, 162.48303222656,3157.2976074219,43.404460906982}
    }
  },
  ["acougue_flat"] = {
    {
      {"entry",965.94787597656,-2187.1303710938,30.157732009888},
      {"chest",973.11932373047,-2187.568359375,29.977527618408, _config = {weight=800}},
      {"wardrobe",990.06939697266,-2175.3447265625,30.0280532836},
      {"gametable",997.65521240234,-2186.8806152344,29.977819442749},
      {"itemtr", _config = itemtr_acoguevip, 993.71453857422,-2159.4138183594,29.476511001587}
    }
  },
  ["PAIN_flat"] = {
    {
      {"entry",3685.4291992188,4937.728515625,22.575679779053},
      {"chest",3700.8825683594,4938.4741210938,22.582185745239, _config = {weight=1500}},
      {"wardrobe",3693.4448242188,4940.42578125,27.140117645264},
      {"gametable",3693.5661621094,4939.6059570313,27.140108108521},
      {"itemtr", _config = itemtr_puteirovip, 3687.8627929688,4957.3935546875,22.575859069824}
    }
  }
}

-- define home clusters
cfg.homes = {
  ["Apartamento Simples 1"] = {
    slot = "basic_flat",
    entry_point = {-618.73333740234,37.5940284729,43.591003417969},
    buy_price = 250000,
    sell_price = 100000,
    max = 99,
    blipid=476,
    blipcolor=4
  },
  ["Apartamento Simples 2"] = {
    slot = "basic_flat",
    entry_point = {-1446.769,-538.531,34.740},
    buy_price = 250000,
    sell_price = 100000,
    max = 99,
    blipid=476,
    blipcolor=4
  },
  ["Barraco Grove 1"] = {
    slot = "Favela_gro1",
    entry_point = {29.70157623291,-1856.5112304688,23.670845031738},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 2"] = {
    slot = "Favela_gro2",
    entry_point = {52.443580627441,-1875.6827392578,22.399784088135},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 3"] = {
    slot = "Favela_gro3",
    entry_point = {105.61461639404,-1920.0891113281,21.059619903564},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 4"] = {
    slot = "Favela_gro4",
    entry_point = {86.694869995117,-1955.2919921875,20.749671936035},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 5"] = {
    slot = "Favela_gro5",
    entry_point = {14.418782234192,-1887.1783447266,23.237241744995},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 6"] = {
    slot = "Favela_gro6",
    entry_point = {483.4560546875,-1700.5466308594,29.351318359375},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 7"] = {
    slot = "Favela_gro7",
    entry_point = {454.63165283203,-1711.4241943359,29.507839202881},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 8"] = {
    slot = "Favela_gro8",
    entry_point = {395.56173706055,-1844.3100585938,26.838708877563},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 9"] = {
    slot = "Favela_gro9",
    entry_point = {351.04852294922,-1884.0579833984,25.14541053772},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Grove 10"] = {
    slot = "Favela_gro10",
    entry_point = {219.63836669922,-1880.1647949219,29.194231033325},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },             
  ["Barraco Helipa 1"] = {
    slot = "Favela_heli1",
    entry_point = {1285.1458740234,-401.84600830078,69.20384979248},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 2"] = {
    slot = "Favela_heli2",
    entry_point = {1303.1389160156,-419.51031494141,68.853889465332},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 3"] = {
    slot = "Favela_heli3",
    entry_point = {1294.3630371094,-494.20935058594,69.458427429199},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 4"] = {
    slot = "Favela_heli4",
    entry_point = {1336.0543212891,-551.39660644531,73.270286560059},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 5"] = {
    slot = "Favela_heli5",
    entry_point = {1330.9041748047,-608.35571289063,74.50806427002},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 6"] = {
    slot = "Favela_heli6",
    entry_point = {1336.9061279297,-577.0322265625,74.129188537598},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 7"] = {
    slot = "Favela_heli7",
    entry_point = {1312.4370117188,-573.85266113281,72.215225219727},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 8"] = {
    slot = "Favela_heli8",
    entry_point = {1345.900390625,-474.72744750977,72.648529052734},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 9"] = {
    slot = "Favela_heli9",
    entry_point = {1336.7810058594,-490.52807617188,72.139228820801},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Helipa 10"] = {
    slot = "Favela_heli10",
    entry_point = {1373.5047607422,-650.11743164063,73.953773498535},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Paraiso 1"] = {
    slot = "Favela_para1",
    entry_point = {-764.55328369141,557.51629638672,128.05978393555},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 2"] = {
    slot = "Favela_para2",
    entry_point = {-743.99554443359,560.99658203125,131.59582519531},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 3"] = {
    slot = "Favela_para3",
    entry_point = {-763.3017578125,536.77270507813,117.10884857178},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 4"] = {
    slot = "Favela_para4",
    entry_point = {-806.47412109375,630.32690429688,129.20426940918},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 5"] = {
    slot = "Favela_para5",
    entry_point = {-958.39807128906,655.59344482422,138.36982727051},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 6"] = {
    slot = "Favela_para6",
    entry_point = {-991.23626708984,647.73645019531,139.65327453613},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 7"] = {
    slot = "Favela_para7",
    entry_point = {-1018.2311401367,645.34222412109,140.99476623535},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 8"] = {
    slot = "Favela_para8",
    entry_point = {-854.92755126953,616.20886230469,115.19501495361},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 9"] = {
    slot = "Favela_para9",
    entry_point = {-792.71881103516,509.90768432617,103.19808197021},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Paraiso 10"] = {
    slot = "Favela_para10",
    entry_point = {-789.14154052734,512.30615234375,104.5364151001},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Rocinha 1"] = {
    slot = "Favela_roci1",
    entry_point = {812.28485107422,-220.58528137207,68.361312866211},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Rocinha 2"] = {
    slot = "Favela_roci2",
    entry_point = {800.33251953125,-267.12225341797,69.93991851806},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Rocinha 3"] = {
    slot = "Favela_roci3",
    entry_point = {769.19860839844,-177.7197265625,74.812881469727},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Rocinha 4"] = {
    slot = "Favela_roci4",
    entry_point = {762.076171875,-206.7777557373,66.114486694336},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Rocinha 5"] = {
    slot = "Favela_roci5",
    entry_point = {665.44940185547,-304.2841796875,43.763652801514},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Barraco Rocinha 6"] = {
    slot = "Favela_roci6",
    entry_point = {658.39770507813,-346.66775512695,34.9121398925781},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Rocinha 7"] = {
    slot = "Favela_roci7",
    entry_point = {856.18988037109,-276.94589233398,65.652740478516},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  },
  ["Barraco Rocinha 8"] = {
    slot = "Favela_roci8",
    entry_point = {731.77478027344,-247.91801452637,66.343124389648},
    buy_price = 80000,
    sell_price = 55000,
    max = 1
  }, 
  ["Casa Vip 1"] = {
    slot = "vip1_flat",
    entry_point = {-175.11683654785,502.34225463867,137.42016601563},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  },
  ["Casa Vip 2"] = {
    slot = "vip2_flat",
    entry_point = {346.84008789063,440.90414428711,147.70207214355},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  },
  ["Casa Vip 3"] = {
    slot = "vip3_flat",
    entry_point = {373.43969726563,427.83197021484,145.68418884277},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  },
  ["Casa Vip 4"] = {
    slot = "vip4_flat",
    entry_point = {-686.41821289063,596.54180908203,143.64221191406},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  },
  ["Casa Vip 5"] = {
    slot = "vip5_flat",
    entry_point = {-1294.3278808594,454.73611450195,97.506507873535},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  }, 
  ["Casa Vip 7"] = {
    slot = "vip7_flat",
    entry_point = {3262.1420898438,-122.79710388184,15.754470825195},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  },
  ["Casa Vip 8"] = {
    slot = "vip8_flat",
    entry_point = {720.32507324219,-924.06353759766,26.776823043823},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  }, 
  ["Casa Vip 9"] = {
    slot = "vip9_flat",
    entry_point = {-817.37890625,178.0503692627,72.227806091309},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  }, 
  ["Casa Vip 10"] = {
    slot = "vip10_flat",
    entry_point = {2845.4404296875,-740.42242431641,6.2502117156982},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  }, 
  ["Casa Vip 11"] = {
    slot = "vip11_flat",
    entry_point = {773.62957763672,-150.34896850586,75.621864318848},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=5
  }, 
  ["MC Vip"] = {
    slot = "thelost_flat",
    entry_point = {982.32745361328,-103.38913726807,74.848731994629},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=378,
    blipcolor=40
  },
  ["Bordel Yakuza"] = {
    slot = "puteiro_flat",
    entry_point = {96.22762298584,-1286.7371826172,29.268760681152},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=121,
    blipcolor=27
  },
  ["Tequi-la-la Vip"] = {
    slot = "tekila_flat",
    entry_point = {-561.85662841797,294.91815185547,87.493598937988},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=136,
    blipcolor=60
  },
  ["Açougue Vip"] = {
    slot = "acougue_flat",
    entry_point = {960.51934814453,-2184.5583496094,30.496740341187},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=136,
    blipcolor=60
  },
  ["Aviario Vip"] = {
    slot = "aviario_flat",
    entry_point = {-74.075073242188,6265.876953125,31.247638702393},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=136,
    blipcolor=60
  }, 
  ["Bonde Dos 13 Vip"] = {
    slot = "PAIN_flat",
    entry_point = {3682.4855957031,4939.3154296875,22.575510025024},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=439,
    blipcolor=46
  },
  ["Fazenda"] = {
    slot = "fazenda_flat",
    entry_point = {1394.7622070313,1141.7633056641,114.61884307861},
    buy_price = 50000000,
    sell_price = 15000,
    max = 1,
    blipid=40,
    blipcolor=4
  },   
  ["Motoclube Caveiras"] = {
    slot = "motoclubecaveiras_flat",
    entry_point = {1022.0795898438,-2318.6240234375,30.509571075439},
    buy_price = 250000,
    sell_price = 150000,
    max = 1,
    blipid=378,
    blipcolor=40
  },    
  ["Filhos da Candelaria"] = {
    slot = "filhodacadelaria_flat",
    entry_point = {164.77127075195,3164.66796875,43.404491424561},
    buy_price = 50000000,
    sell_price = 150000,
    max = 1,
    blipid=378,
    blipcolor=40
  },  
  ["Motoclube Lobos do Deserto"] = {
    slot = "motoclube_flat",
    entry_point = {1991.0048828125,3054.2280273438,47.214920043945},
    buy_price = 250000,
    sell_price = 150000,
    max = 1,
    blipid=442,
    blipcolor=4
  },  
  ["Apartamento de luxo"] = {
    slot = "other_flat",
    entry_point = {-770.921,312.537,85.698},
    buy_price = 850000,
    sell_price = 650000,
    max = 10,
    blipid=475,
    blipcolor=5
  },
  ["Casa Simples Bonde Dos 13"] = {
    slot = "basic_flat",
    entry_point = {3619.5979003906,5015.130859375,11.152397155762},
    buy_price = 50000000,
    sell_price = 100000,
    max = 99,
    blipid=476,
    blipcolor=4
  }

}

return cfg
