
local cfg = {}

-- PCs positions
cfg.pcs = {
  {442.04299926758, -977.76538085938, 30.689607620239},
  {378.06359863281,-1616.8461914063,19.167358398438},
  { 327.46499633789,-1615.2686767578,19.145708084106}
}

-- vehicle tracking configuration
cfg.trackveh = {
  min_time = 300, -- min time in seconds
  max_time = 600, -- max time in seconds
  service = "police" -- service to alert when the tracking is successful
}

-- wanted display
cfg.wanted = {
  blipid = 458,
  blipcolor = 38,
  service = "police"
}

-- illegal items (seize)
cfg.seizable_items = {
  "dirty_money",
  "cannabis",
  "cacaina",
  "folhadecoca",
  "metanfetamina",
  "heroina",
  "crack",
  "lanca",
  "crystalmelamine",
  "opio",
  "cristal",
  "cloreto",
  "WEAPON_SPECIALCARBINE",
  "WEAPON_ASSAULTRIFLE",
  "WEAPON_MARKSMANRIFLE",
  "WEAPON_COMBATPISTOL",
  "WEAPON_DBSHOTGUN",
  "WEAPON_ASSAULTSMG",
  "WEAPON_SMG",
  "WEAPON_PUMPSHOTGUN",
  "dinheiro_roubo",
  "AK47",
  "M4A1",
  "WEAPON_ADVANCEDRIFLE",
  "WEAPON_MARKSMANPISTOL",
  "weed"
}

-- jails {x,y,z,radius}
cfg.jails = {
  {459.485870361328,-1001.61560058594,24.914867401123,2.1},
  {459.305603027344,-997.873718261719,24.914867401123,2.1},
  {459.999938964844,-994.331298828125,24.9148578643799,1.6}
}

-- fines
-- map of name -> money
cfg.fines = {    
  ["1.1 500"] = 500,
  ["1.2 1000"] = 1000,
  ["1.3 5000"] = 5000,
  ["1.4 10000"] = 10000,
  ["1.5 20000"] = 20000,
  ["1.6 50000"] = 50000,
  ["1.7 100000"] = 100000,
  ["1.8 Grave 1"] = 0,
  ["1.9 Grave 2"] = 0,
  ["2.1 Grave 3"] = 0,
}

return cfg
