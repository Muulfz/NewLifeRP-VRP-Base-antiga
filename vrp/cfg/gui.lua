
-- gui config file
local cfg = {}

-- additional css loaded to customize the gui display (see gui/design.css to know the available css elements)
-- it is not recommended to modify the vRP core files outside the cfg/ directory, create a new resource instead
-- you can load external images/fonts/etc using the NUI absolute path: nui://my_resource/myfont.ttf
-- example, changing the gui font (suppose a vrp_mod resource containing a custom font)
cfg.css = [[
@font-face {
  font-family: "pcdown";
  src: url(nui://vrp/gui/fonts/pcdown.ttf) format("truetype");
}

@font-face {
  font-family: "bankgothic";
  src: url(nui://vrp/gui/fonts/bankgothic.ttf) format("truetype");
}

body{
  font-family: "Segoe UI";
  font-size: 0.9em;
}
]]

-- list of static menu types (map of name => {.title,.blipid,.blipcolor,.permissions (optional)})
-- static menus are menu with choices defined by vRP.addStaticMenuChoices(name, choices)
cfg.static_menu_types = {
   ["police_weapons"] = {
      title = "Armas Policial", 
      blipcolor = 0,
	  permissions = {
	    "police.weapons"
	  }
	},
   ["rota_weapons"] = {
      title = "Armas Policial", 
      blipcolor = 0,
    permissions = {
      "rota.weapons"
    }
  },
   ["pmft_weapons"] = {
      title = "Armas Policial", 
      blipcolor = 0,
    permissions = {
      "pmft.weapons"
    }
  },
   ["pcivil_weapons"] = {
      title = "Armas Policial", 
      blipcolor = 0,
    permissions = {
      "pcivil_weapons"
    }
  },
   ["garra_weapons"] = {
      title = "Armas Policial - GARRA", 
      blipcolor = 0,
    permissions = {
      "garra.weapons"
    }
  },
   ["exercito_weapons"] = {
      title = "Armas para limpar quintal", 
      blipcolor = 0,
    permissions = {
      "exercito.weapons"
    }
  },
   ["emergency_heal"] = {
      title = "Atendimento Médico", 
      blipcolor = 0,
	  permissions = {
	    "emergency_heal"
	  }
	},
   ["emergency_medkit"] = {
      title = "Emergência", 
      blipcolor = 0,
	  permissions = {
	    "emergency.medkit"
	  }
	},
  ["missoes"] = { -- example of a mission menu that can be filled by other resources
    title = "Missoes",
    blipid = 205, 
    blipcolor = 5
  }
	
}

-- list of static menu points
cfg.static_menus = {
  {"police_weapons", 397.48709106445,-1610.3175048828,19.167369842529},
  {"pmft_weapons", 397.48709106445,-1610.3175048828,19.167369842529},
  {"rota_weapons", 351.48681640625,-1611.1232910156,19.145702362061},
  {"pmft_weapons", 351.48681640625,-1611.1232910156,19.145702362061},
  {"pcivil_weapons", 461.31414794922,-981.15582275391,30.689588546753},
  {"garra_weapons", 461.31414794922,-981.15582275391,30.689588546753},
  {"exercito_weapons", 461.31414794922,-981.15582275391,30.689588546753},
  {"emergency_medkit", 268.22784423828,-1364.8872070313,24.537782669067},
  {"emergency_heal", 260.49597167969,-1358.4555664063,24.537788391113},
  {"missoes", 1855.13940429688,3688.68579101563,34.2670478820801},
  {"missoes", -252.389709472656,-972.658874511719,31.2200050354004}  
}


return cfg
