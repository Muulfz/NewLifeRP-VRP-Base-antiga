
local cfg = {}

-- define customization parts
local parts = {
  ["Skins"] = 0,
  ["Desbugar Fardas"] = 3,
  ["Pernas"] = 4,
  ["Capacetes/Boinas"] = "p0",
  ["Oculos"] = "p1",
}

-- changes prices (any change to the character parts add amount to the total price)
cfg.drawable_change_price = 0
cfg.texture_change_price = 0


-- skinshops list {parts,x,y,z}
cfg.desbugskins = {
  {parts,450.41436767578,-992.41040039063,30.689598083496},
  {parts,401.2399597168,-1610.2161865234,19.167358398438},
  {parts,350.31259155273,-1615.9451904297,19.145708084106}
}

return cfg
