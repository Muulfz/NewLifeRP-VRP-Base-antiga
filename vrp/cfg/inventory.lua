
local cfg = {}

cfg.inventory_weight_per_strength = 10 -- weight for an user inventory per strength level (no unit, but thinking in "kg" is a good norm)

-- default chest weight for vehicle trunks
cfg.default_vehicle_chest_weight = 50

-- define vehicle chest weight by model in lower case
cfg.vehicle_chest_weights = {
  ["monster"] = 250,
  ["f812"] = 100,
  ["gtcl"] = 100,
  ["xj"] = 100,
  ["p7"] = 100,
  ["720s"] = 100,
  ["mp412c"] = 100,
  ["macan"] = 100,
  ["panamera17turbo"] = 100,
  ["chiron17"] = 100,
  ["fpace2017"] = 100,
  ["wrangler86"] = 100,
  ["rmodmustang"] = 100,
  ["amggts"] = 100,
  ["g65amg"] = 100,
  ["mlbrabus"] = 100,
  ["rmodm4gts"] = 100,
  ["c7r"] = 100,
  ["apollos"] = 100,
  ["rcf"] = 100,
  ["918"] = 100,
  ["88gtb"] = 100,
  ["ferrari812"] = 100,
  ["gtr"] = 100,
  ["d1r34"] = 100,
  ["SubaruGT"] = 100,
  ["eg6"] = 100,
  ["skyline"] = 100,
  ["BMX"] = 10,
  ["cruiser"] = 10,
  ["f430s"] = 100,
  ["gt17"] = 100,
  ["w221s500"] = 100,
  ["rmodpagani"] = 100,
  ["p911r"] = 100
}

return cfg
