cfg = {}

cfg.blips = false -- enable blips

cfg.seconds = 480 -- seconds to rob

cfg.cooldown = 3600 -- time between robbaries

cfg.cops = 5 -- minimum cops online
cfg.permission = "bank.police" -- permission given to cops

cfg.banks = { -- list of banks
	["caixaeco"] = {
		position = { ['x'] = 147.04908752441, ['y'] = -1044.9448242188, ['z'] = 29.36802482605 },
		reward = 30000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Praça do Passificador)",
		lastrobbed = 0
	},
	["caixaeco2"] = {
		position = { ['x'] = -2957.6674804688, ['y'] = 481.45776367188, ['z'] = 15.697026252747 },
		reward = 30000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Praião)",
		lastrobbed = 0
	},
	["caixapoup"] = {
		position = { ['x'] = -107.06505584717, ['y'] = 6474.8012695313, ['z'] = 31.62670135498 },
		reward = 30000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Engenhanápolis)",
		lastrobbed = 0
	},
	["caixaeco3"] = {
		position = { ['x'] = -1212.2568359375, ['y'] = -336.128295898438, ['z'] = 36.7907638549805 },
		reward = 30000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Alta)",
		lastrobbed = 0
	},
	["caixaeco4"] = {
		position = { ['x'] = -354.452575683594, ['y'] = -53.8204879760742, ['z'] = 48.0463104248047 },
		reward = 30000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Shopping)",
		lastrobbed = 0
	},
	["caixaeco5"] = {
		position = { ['x'] = 309.967376708984, ['y'] = -283.033660888672, ['z'] = 53.1745223999023 },
		reward = 30000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Baixa)",
		lastrobbed = 0
	},
	["caixaeco6"] = {
		position = { ['x'] = 1176.86865234375, ['y'] = 2711.91357421875, ['z'] = 38.097785949707 },
		reward = 30000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Interior)",
		lastrobbed = 0
	},
	["bancoprincipal"] = {
		position = { ['x'] = 265.60995483398, ['y'] = 213.61218261719, ['z'] = 101.68347930908 },
		reward = 60000 + math.random(100000,200000),
		nameofbank = "Caixa Economica (Principal)",
		lastrobbed = 0
	},
    ["rockford_jewlery"] = {
        position = { ['x'] = -621.989135742188, ['y'] = -230.804443359375, ['z'] = 38.0570297241211 },
        reward = 1000 + math.random(120000,280000),
        nameofbank = "Joalheria. (Vangelico)",
        lastrobbed = 0	
	}
}