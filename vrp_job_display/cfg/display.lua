
local cfg = {}

cfg.firstjob = false -- set this to your first job, for example "citizen", or false to disable

-- text display css
cfg.display_css = [[
.div_job{
  position: absolute;
  top: 155px;
  right: 20px;
  font-size: 20px;
  font-weight: bold;
  color: white;
  text-shadow: 3px 3px 2px rgba(0, 0, 0, 0.80);
}
]]

-- icon display css
cfg.icon_display_css = [[
.div_job_icon{
  position: absolute;
}
]]

-- list of ["group"] => css for icons
cfg.group_icons = {
  ["[Recruta] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
	    top: 180px;
      right: 20px;
    }
  ]],
  ["[Soldado] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Cabo] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[3° Sargento] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Sargento] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Sargento] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Sub. Tenente] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Tenente] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Tenente] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Capitão] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Major] - Polícia Militar"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Recruta] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Soldado] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Cabo] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[3° Sargento] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Sargento] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Sargento] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Sub. Tenente] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Tenente] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Tenente] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Capitão] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Major] - Força Tática"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Recruta] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Soldado] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Cabo] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[3° Sargento] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Sargento] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Sargento] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Sub. Tenente] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Tenente] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Tenente] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Capitão] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Major] - ÁGUIA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Recruta] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Soldado] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Cabo] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[3° Sargento] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Sargento] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Sargento] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Sub. Tenente] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Tenente] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Tenente] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Capitão] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Major] - ROCAM"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Recruta] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Soldado] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Cabo] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[3° Sargento] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Sargento] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Sargento] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Sub. Tenente] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[2° Tenente] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[1° Tenente] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Capitão] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Major] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Tenente.Coronel] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Coronel] - ROTA"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Tenente.Coronel]"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["[Coronel]"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Paramédico"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/SAMU.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Mecânico"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Mecanico.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Taxista"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Taxi.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Entregador"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Caixa.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Ladrão de Carros"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/LadraoDeCarros.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Traficante de Maconha"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Maconha.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Traficante de Metanfetamina"] = [[
    .div_job_icon{
      content: url();
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Traficante de Cocaina"] = [[
    .div_job_icon{
      content: url();
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Traficante de Heroina"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Heroina.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Traficante de Armas"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Armas.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Assassino Profissional"] = [[
    .div_job_icon{
      content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["hacker"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Hacker.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["mugger"] = [[
    .div_job_icon{
      content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Jornalista"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Jornal.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Editor Chefe"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Jornalista.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Advogado"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Advogado.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Transportador de Valores"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Valores.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  ["Desempregado"] = [[
    .div_job_icon{
      content: url(http://brasilnewliferp.com/img/Desempregado.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }    
  ]], -- this is an example, add more under it using the same model, leave the } at the end.
}
return cfg

