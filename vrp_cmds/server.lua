local cfg = module("vrp_cmds", "cfg/commands")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")
vRPcmd = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_cmds")
CMDclient = Tunnel.getInterface("vrp_cmds","vrp_cmds")
Tunnel.bindInterface("vrp_cmds",vRPcmd)

AddEventHandler("chatMessage", function(p, color, msg)
    if msg:sub(1, 1) == "/" then
        text = splitString(msg, " ")
        cmd = text[1]
		args = text[2]
		for k,v in ipairs(text) do
          if k > 2 then
		    args = args.." "..v
          end
		end
		for k,v in pairs(cfg.commands) do
          if cmd == k then
		    v.action(p,color,args)
        	CancelEvent()
          end
		end
    end
end)