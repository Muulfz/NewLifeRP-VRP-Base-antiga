cfg = {}
-- THIS FILE IS ON SERVER SIDE ( Just reminding you ;) )
--[[ Create Your commands inside the list cfg.commands like so: 
  ["/cmd"] = {
    action = function(p,color,msg)
	  -- function of what the command does
	end
  },
]]
-- p is player, color is {r, g, b} of the message and msg is the message of course.
cfg.commands = {
  ["/pos"] = {
    -- /pos to log postion to file with user name and msg
	action = function(p,color,msg) 
	  CMDclient.getPlayerPosH(p,{},function(x,y,z,h)
	    file = io.open("cmdPos.txt", "a")
		if file then
		  file:write(GetPlayerName(p).." at ".."{" .. x .. "," .. y .. "," .. z .. "," .. h .. "} wrote: "..(msg or "").."\n")
		end
		file:close()
		TriggerClientEvent('chatMessage', p, "SYSTEM", {255, 0, 0}, "Location sent to file!")
	  end)
	end
  },
  --HERE GOES YOUR COMMANDS
  
}

return cfg