
vRPcmd = {}
Tunnel.bindInterface("vrp_cmds",vRPcmd)
vRPserver = Tunnel.getInterface("vRP","vrp_cmds")
CMDserver = Tunnel.getInterface("vrp_cmds","vrp_cmds")
vRP = Proxy.getInterface("vRP")

function vRPcmd.getPlayerPosH()
	x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
	local h = GetEntityHeading(GetPlayerPed(-1))
	return x , y , z , h
end