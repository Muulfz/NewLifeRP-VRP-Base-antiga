local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_holdup")

local stores = cfg.holdups

local robbers = {}

function get3DDistance(x1, y1, z1, x2, y2, z2)
	return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2) + math.pow(z1 - z2, 2))
end

RegisterServerEvent('es_holdup:toofar')
AddEventHandler('es_holdup:toofar', function(robb)
	if(robbers[source])then
		TriggerClientEvent('es_holdup:toofarlocal', source)
		robbers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'AVISO', {255, 0, 0}, "Roubo foi cancelado at: ^5" .. stores[robb].nameofstore)
	end
end)

RegisterServerEvent('es_holdup:playerdied')
AddEventHandler('es_holdup:playerdied', function(robb)
	if(robbers[source])then
		TriggerClientEvent('es_holdup:playerdiedlocal', source)
		robbers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'AVISO', {255, 0, 0}, "Roubo foi cancelado at: ^5" .. stores[robb].nameofstore)
	end
end)

RegisterServerEvent('es_holdup:rob')
AddEventHandler('es_holdup:rob', function(robb)
  local user_id = vRP.getUserId({source})
  local player = vRP.getUserSource({user_id})
  local cops = vRP.getUsersByPermission({cfg.permission})
  if vRP.hasPermission({user_id,cfg.permission}) then
    vRPclient.notify(player,{"~r~Policias nao podem roubar lojas."})
  else
    if #cops >= cfg.cops then
	  if stores[robb] then
		  local store = stores[robb]

		  if (os.time() - store.lastrobbed) <  cfg.seconds+cfg.cooldown and store.lastrobbed ~= 0 then
			  TriggerClientEvent('chatMessage', player, 'ROUBADA', {255, 0, 0}, "Essa loja foi roubada recentemente, agurade: ^5" .. (3000 - (os.time() - store.lastrobbed)) .. "^0 segundo.")
			  return
		  end
		  TriggerClientEvent('chatMessage', -1, 'AVISO', {255, 0, 0}, "Assalto a loja em Andamento em ^5" .. store.nameofstore)
		  TriggerClientEvent('chatMessage', player, 'SERVIDOR', {255, 0, 0}, "Voce iniciou um Assalto a loja: ^5" .. store.nameofstore .. "^0, não se afaste desse ponto!")
		  TriggerClientEvent('chatMessage', player, 'SERVIDOR', {255, 0, 0}, "Segure a loja por  ^12 ^0minutos e o dinheiro será seu!")
		  TriggerClientEvent('es_holdup:currentlyrobbing', player, robb)
		  stores[robb].lastrobbed = os.time()
		  robbers[player] = robb
		  local savedSource = player
		  SetTimeout(cfg.seconds*1000, function()
			  if(robbers[savedSource])then
				  if(user_id)then
					  vRP.giveInventoryItem({user_id,"dinheiro_roubo",store.reward,true})
					  TriggerClientEvent('chatMessage', -1, 'AVISO', {255, 0, 0}, "O Roubo da loja: ^5" .. store.nameofstore .. "^0! foi concluido, os Policiais deixaram os Bandidos Escaparem.")
					  TriggerClientEvent('es_holdup:robberycomplete', savedSource, store.reward)
				  end
			  end
		  end)		
	  end
    else
      vRPclient.notify(player,{"~r~Not enough cops online."})
    end
  end
end)