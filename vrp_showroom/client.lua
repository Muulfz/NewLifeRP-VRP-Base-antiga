
vRP = Proxy.getInterface("vRP")
vRPg = Proxy.getInterface("vRP_garages")

function deleteVehiclePedIsIn()
  local v = GetVehiclePedIsIn(GetPlayerPed(-1),false)
  SetVehicleHasBeenOwnedByPlayer(v,false)
  Citizen.InvokeNative(0xAD738C3085FE7E11, v, false, true) -- set not as mission entity
  SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(v))
  Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(v))
end

local vehshop = {
	opened = false,
	title = "Concessionária de Veiculos",
	currentmenu = "main",
	lastmenu = nil,
	currentpos = nil,
	selectedbutton = 0,
	marker = { r = 0, g = 155, b = 255, a = 200, type = 1 },
	menu = {
		x = 0.1,
		y = 0.08,
		width = 0.2,
		height = 0.04,
		buttons = 10,
		from = 1,
		to = 10,
		scale = 0.4,
		font = 0,
		["main"] = {
			title = "Concessionária NewLife",
			name = "main",
			buttons = {
				{name = "vehicles", description = ""},
				{name = "motorcycles", description = ""},
			}
		},
		["vehicles"] = {
			title = "Veiculos",
			name = "vehicles",
			buttons = {
				{name = "populares", description = ''},
				{name = "vips", description = ''},
				{name = "compacto", description = ''},
				{name = "cupe", description = ''},
				{name = "sedans", description = ''},
				{name = "esportivo", description = ''},
				{name = "esportivoclasico", description = ''},
				{name = "supers", description = ''},
				{name = "muscle", description = ''},
				{name = "offroad", description = ''},
				{name = "suvs", description = ''},
				{name = "vans", description = ''},
				{name = "caminhao", description = ''},
				{name = "drift", description = ''},
				--{name = "cycles", description = ''},
			}
		},
		["motorcycles"] = {
			title = "Motos",
			name = "motorcycles",
			buttons = {
				--[[{name = "BMX", description = {}, model = "BMX"},
				{name = "Bicicleta de Passeio", costs = 320, description = {}, model = "cruiser"},
				{name = "Bagger", costs = 5000, description = {}, model = "bagger"},
				{name = "BMW S1000RR", costs = 91000, description = {}, model = "bmws"},
				{name = "Kawasaki Ninja HR2", costs = 89100, description = {}, model = "nh2r"},
				{name = "Honda CBR 1000RR", costs = 89100, description = {}, model = "hcbr17"},
				{name = "Kawasaki ZX10R", costs = 84641, description = {}, model = "zx10r"},
				{name = "Yamaha YZF R1 2015", costs = 80950, description = {}, model = "r1v2"},
				{name = "Yamaha R6 - 2017", costs = 67100, description = {}, model = "r6"},
				{name = "KTM  RC390 - 2014", costs = 53100, description = {}, model = "rc"},
				{name = "Yamaha MT10", costs = 78100, description = {}, model = "mt10"},
				{name = "Ducatti Multilistrada", costs = 50000000, description = {}, model = "dm1200"},
				{name = "Ducatti 1199 Panigalle", costs = 50000000, description = {}, model = "d99"},
				{name = "Honda Titan 160", costs = 43000, description = {}, model = "titan60"},
				{name = "Yamaha XT660R Enduro", costs = 42000, description = {}, model = "xt66"},
				{name = "Yamaha MT09", costs = 84641, description = {}, model = "mt09"},]]
			}
		},
		["populares"] = {
			title = "Populares",
			name = "populares",
			buttons = {
				--[[{name = "Volkswagen Fusca", costs = 7100, description = {}, model = "fulux63"},
				{name = "Chevrolet Chevette", costs = 15100, description = {}, model = "chevette"},
				{name = "Citroen c4", costs = 48000, description = {}, model = "citroc4"},
				{name = "Pegout 206", costs = 72000, description = {}, model = "206"},
				{name = "Ford Ka", costs = 80000, description = {}, model = "fordka"},
				{name = "Volkswagen Golf GTI Mk7", costs = 81000, description = {}, model = "mk7"},
				{name = "Honda Civic Hatchback", costs = 87100, description = {}, model = "honda"},
				{name = "Volkswagen Golf", costs = 174100, description = {}, model = "golf6"},]]
			}
		},
		["vips"] = {
			title = "Luxo Vip",
			name = "vips",
			buttons = {
				--[[{name = "Ford GT 2017", costs = 50000000, description = {}, model = "gt17"},
				{name = "BMW 330i", costs = 50000000, description = {}, model = "e90"},
				{name = "Mclaren 720", costs = 50000000, description = {}, model = "720s"},
				{name = "Mclaren MP4", costs = 50000000, description = {}, model = "mp412c"},
				{name = "Porsche Macan", costs = 50000000, description = {}, model = "macan"},
				{name = "Porsche 911", costs = 50000000, description = {}, model = "p911r"},
				{name = "Porsche Panamera", costs = 50000000, description = {}, model = "panamera17turbo"},
				{name = "Porsche 918 Spider", costs = 50000000, description = {}, model = "918"},
				{name = "Bugatti Chiron", costs = 50000000, description = {}, model = "chiron17"},
				{name = "XJ2010 Jaguar", costs = 50000000, description = {}, model = "xj"},
				{name = "PRanger Rover Sport SVR 2016", costs = 50000000, description = {}, model = "rsvr16"},
				{name = "Jaguar Project7", costs = 50000000, description = {}, model = "p7"},
				{name = "Jaguar F PACE", costs = 50000000, description = {}, model = "fpace2017"},
				{name = "Jeep Wrangler", costs = 50000000, description = {}, model = "wrangler86"},
				{name = "Mustang GT", costs = 50000000, description = {}, model = "rmodmustang"},
				{name = "Mercedes Benz AMGGTS", costs = 50000000, description = {}, model = "amggts"},
				{name = "Mercedes Benz S5001", costs = 50000000, description = {}, model = "w221s500"},
				{name = "Mercedes-Benz G65", costs = 50000000, description = {}, model = "g65amg"},
				{name = "Mercedes Benz MLBrabus 2009", costs = 50000000, description = {}, model = "mlbrabus"},
				{name = "2016 BMW M4 GTS", costs = 50000000, description = {}, model = "rmodm4gts"},
				{name = "Chevrolet Corvette c7r", costs = 50000000, description = {}, model = "c7r"},
				{name = "Gumpert Apollo", costs = 50000000, description = {}, model = "apollos"},
				{name = "Lexus RC F", costs = 50000000, description = {}, model = "rcf"},
				{name = "Ferrari GTC 4 Lusso", costs = 50000000, description = {}, model = "gtcl"},
				{name = "Ferrari 488", costs = 50000000, description = {}, model = "488gtb"},
				{name = "Ferrari812", costs = 50000000, description = {}, model = "ferrari812"},
				{name = "Lamborghini Hurucan", costs = 50000000, description = {}, model = "lp610"},
				{name = "Pagani Huayra", costs = 50000000, description = {}, model = "rmodpagani"},]]
			}
		},
		["compacto"] = {
			title = "Compacto",
			name = "compacto",
			buttons = {
			--[[	{name = "Hyundai Veloster", costs = 178100, description = {}, model = "veloster"},
				{name = "Blista", costs = 15000, description = {}, model = "blista"},
				{name = "Brioso R/A", costs = 155000, description = {}, model = "brioso"},
				{name = "Dilettante", costs = 25000, description = {}, model = "Dilettante"},
				{name = "Issi", costs = 18000, description = {}, model = "issi2"},
				{name = "Panto", costs = 85000, description = {}, model = "panto"},
				{name = "Prairie", costs = 30000, description = {}, model = "prairie"},
				{name = "Rhapsody", costs = 120000, description = {}, model = "rhapsody"},--]]
			}
		},
		["cupe"] = {
			title = "Cupê",
			name = "cupe",
			buttons = {
			--[[	{name = "Cognoscenti Cabrio", costs = 250000, description = {}, model = "cogcabrio"},
			    {name = "Exemplar", costs = 200000, description = {}, model = "exemplar"},
			    {name = "F620", costs = 80000, description = {}, model = "f620"},
			    {name = "Felon", costs = 90000, description = {}, model = "felon"},
			    {name = "Felon GT", costs = 95000, description = {}, model = "felon2"},
			    {name = "Jackal", costs = 60000, description = {}, model = "jackal"},
			    {name = "Oracle", costs = 80000, description = {}, model = "oracle"},
			    {name = "Oracle XS", costs = 82000, description = {}, model = "oracle2"},
			    {name = "Sentinel", costs = 90000, description = {}, model = "sentinel"},
			    {name = "Sentinel XS", costs = 60000, description = {}, model = "sentinel2"},
			    {name = "Windsor", costs = 800000, description = {}, model = "windsor"},
			    {name = "Windsor Drop", costs = 850000, description = {}, model = "windsor2"},
			    {name = "Zion", costs = 60000, description = {}, model = "zion"},
			    {name = "Zion Cabrio", costs = 65000, description = {}, model = "zion2"},--]]
			}
		},
		["esportivo"] = {
			title = "esportivo",
			name = "esportivo",
			buttons = {
			--[[	{name = "Camaro Zl1", costs = 435000, description = {}, model = "czl1"},
				{name = "9F", costs = 150000, description = {}, model = "ninef"},
			    {name = "9F Cabrio", costs = 130000, description = {}, model = "ninef2"},
			    {name = "Alpha", costs = 150000, description = {}, model = "alpha"},
			    {name = "Banshee", costs = 105000, description = {}, model = "banshee"},
			    {name = "Bestia GTS", costs = 610000, description = {}, model = "bestiagts"},
			    {name = "Blista Compact", costs = 42000, description = {}, model = "blista"},
			    {name = "Buffalo", costs = 35000, description = {}, model = "buffalo"},
			    {name = "Buffalo S", costs = 96000, description = {}, model = "buffalo2"},
			    {name = "Carbonizzare", costs = 195000, description = {}, model = "carbonizzare"},
			    {name = "Comet", costs = 100000, description = {}, model = "comet2"},
			    {name = "Coquette", costs = 138000, description = {}, model = "coquette"},
			    {name = "Drift Tampa", costs = 995000, description = {}, model = "tampa2"},
			    {name = "Feltzer", costs = 130000, description = {}, model = "feltzer2"},
			    {name = "Furore GT", costs = 448000, description = {}, model = "furoregt"},
			    {name = "Fusilade", costs = 36000, description = {}, model = "fusilade"},
			    {name = "Jester", costs = 240000, description = {}, model = "jester"},
			    {name = "Jester(Racecar)", costs = 350000, description = {}, model = "jester2"},
			    {name = "Kuruma", costs = 95000, description = {}, model = "kuruma"},
			    {name = "Lynx", costs = 1735000, description = {}, model = "lynx"},
			    {name = "Massacro", costs = 275000, description = {}, model = "massacro"},
			    {name = "Massacro(Racecar)", costs = 385000, description = {}, model = "massacro2"},
			    {name = "Omnis", costs = 701000, description = {}, model = "omnis"},
			    {name = "Penumbra", costs = 24000, description = {}, model = "penumbra"},
			    {name = "Rapid GT", costs = 140000, description = {}, model = "rapidgt"},
			    {name = "Rapid GT Convertible", costs = 150000, description = {}, model = "rapidgt2"},
			    {name = "Schafter V12", costs = 140000, description = {}, model = "schafter3"},
			    {name = "Sultan", costs = 12000, description = {}, model = "sultan"},
			    {name = "Surano", costs = 110000, description = {}, model = "surano"},
			    {name = "Tropos", costs = 816000, description = {}, model = "tropos"},
			    {name = "Verkierer", costs = 695000, description = {}, model = "verlierer2"},--]]
			}
		},
		["esportivoclasico"] = {
			title = "Esportivo Classico",
			name = "esportivoclasico",
			buttons = {
			--[[	{name = "Casco", costs = 720000, description = {}, model = "casco"},
			    {name = "Coquette Classic", costs = 665000, description = {}, model = "coquette2"},
			    {name = "JB 700", costs = 350000, description = {}, model = "jb700"},
			    {name = "Pigalle", costs = 400000, description = {}, model = "pigalle"},
			    {name = "Stinger", costs = 850000, description = {}, model = "stinger"},
			    {name = "Stinger GT", costs = 875000, description = {}, model = "stingergt"},
			    {name = "Stirling GT", costs = 975000, description = {}, model = "feltzer3"},
			    {name = "Z-Type", costs = 950000, description = {}, model = "ztype"},--]]
			}
		},
		["supers"] = {
			title = "supers",
			name = "supers",
			buttons = {
			--[[	{name = "Lamborghini Countach", costs = 1060000, description = {}, model = "count5"},
				{name = "Adder", costs = 1200000, description = {}, model = "adder"},
			    {name = "Banshee 900R", costs = 565000, description = {}, model = "banshee2"},
			    {name = "Bullet", costs = 155000, description = {}, model = "bullet"},
			    {name = "Cheetah", costs = 650000, description = {}, model = "cheetah"},
			    {name = "Entity XF", costs = 795000, description = {}, model = "entityxf"},
			    {name = "ETR1", costs = 199500, description = {}, model = "sheava"},
			    {name = "FMJ", costs = 1750000, description = {}, model = "fmj"},
			    {name = "Infernus", costs = 440000, description = {}, model = "infernus"},
			    {name = "Osiris", costs = 1950000, description = {}, model = "osiris"},
			    {name = "RE-7B", costs = 2475000, description = {}, model = "le7b"},
			    {name = "Reaper", costs = 1595000, description = {}, model = "reaper"},
			    {name = "Sultan RS", costs = 795000, description = {}, model = "sultanrs"},
			    {name = "T20", costs = 2200000, description = {}, model = "t20"},
			    {name = "Turismo R", costs = 500000, description = {}, model = "turismor"},
			    {name = "Tyrus", costs = 2550000, description = {}, model = "tyrus"},
			    {name = "Vacca", costs = 240000, description = {}, model = "vacca"},
			    {name = "Voltic", costs = 150000, description = {}, model = "voltic"},
			    {name = "X80 Proto", costs = 2700000, description = {}, model = "prototipo"},
			    {name = "Zentorno", costs = 725000, description = {}, model = "zentorno"},--]]
			}
		},
		["muscle"] = {
			title = "muscle",
			name = "muscle",
			buttons = {
			--[[	{name = "Blade", costs = 220000, description = {}, model = "blade"},
			    {name = "Buccaneer", costs = 29000, description = {}, model = "buccaneer"},
			    {name = "Chino", costs = 225000, description = {}, model = "chino"},
			    {name = "Coquette BlackFin", costs = 695000, description = {}, model = "coquette3"},
			    {name = "Dominator", costs = 35000, description = {}, model = "dominator"},
			    {name = "Dukes", costs = 62000, description = {}, model = "dukes"},
			    {name = "Gauntlet", costs = 32000, description = {}, model = "gauntlet"},
			    {name = "Hotknife", costs = 90000, description = {}, model = "hotknife"},
			    {name = "Faction", costs = 36000, description = {}, model = "faction"},
			    {name = "Nightshade", costs = 585000, description = {}, model = "nightshade"},
			    {name = "Picador", costs = 9000, description = {}, model = "picador"},
			    {name = "Sabre Turbo", costs = 15000, description = {}, model = "sabregt"},
			    {name = "Tampa", costs = 375000, description = {}, model = "tampa"},
			    {name = "Virgo", costs = 195000, description = {}, model = "virgo"},
			    {name = "Vigero", costs = 21000, description = {}, model = "vigero"},--]]
			}
		},
		["offroad"] = {
			title = "offroad",
			name = "offroad",
			buttons = {
			--[[	{name = "Bifta", costs = 750000, description = {}, model = "bifta"},
			    {name = "Blazer", costs = 8000, description = {}, model = "blazer"},
			    {name = "Brawler", costs = 715000, description = {}, model = "brawler"},
			    {name = "Bubsta 6x6", costs = 249000, description = {}, model = "dubsta3"},
			    {name = "Dune Buggy", costs = 20000, description = {}, model = "dune"},
			    {name = "Rebel", costs = 22000, description = {}, model = "rebel2"},
			    {name = "Sandking", costs = 38000, description = {}, model = "sandking"},
			    {name = "The Liberator", costs = 550000, description = {}, model = "monster"},
			    {name = "Trophy Truck", costs = 550000, description = {}, model = "trophytruck"},--]]
			}
		},
		["suvs"] = {
			title = "suvs",
			name = "suvs",
			buttons = {
			--[[	{name = "Volkswagen Taureg", costs = 79101, description = {}, model = "r50"},
				{name = "Hyundai Santa fé", costs = 195000, description = {}, model = "santafe"},
			    {name = "Baller", costs = 90000, description = {}, model = "baller"},
			    {name = "Cavalcade", costs = 60000, description = {}, model = "cavalcade"},
			    {name = "Grabger", costs = 35000, description = {}, model = "granger"},
			    {name = "Huntley S", costs = 195000, description = {}, model = "huntley"},
			    {name = "Landstalker", costs = 58000, description = {}, model = "landstalker"},
			    {name = "Radius", costs = 32000, description = {}, model = "radi"},
			    {name = "Rocoto", costs = 85000, description = {}, model = "rocoto"},
			    {name = "Seminole", costs = 30000, description = {}, model = "seminole"},
			    {name = "XLS", costs = 253000, description = {}, model = "xls"},--]]
			}
		},
		["vans"] = {
			title = "vans",
			name = "vans",
			buttons = {
			--[[	{name = "Ford Transit", costs = 182000, description = {}, model = "ford"},
				{name = "Bison", costs = 300000, description = {}, model = "bison"},
			    {name = "Bobcat XL", costs = 23000, description = {}, model = "bobcatxl"},
			    {name = "Gang Burrito", costs = 65000, description = {}, model = "gburrito"},
			    {name = "Journey", costs = 15000, description = {}, model = "journey"},
			    {name = "Minivan", costs = 30000, description = {}, model = "minivan"},
			    {name = "Paradise", costs = 25000, description = {}, model = "paradise"},
			    {name = "Rumpo", costs = 13000, description = {}, model = "rumpo"},
			    {name = "Surfer", costs = 11000, description = {}, model = "surfer"},
			    {name = "Youga", costs = 16000, description = {}, model = "youga"},--]]
			}
		},
		["sedans"] = {
			title = "sedans",
			name = "sedans",
			buttons = {
			--[[ 	{name = "Renault Megane", costs = 55100, description = {}, model = "megane"},				
				{name = "Toyota Camry 2016 ", costs = 149100, description = {}, model = "camry55"},	
				{name = "Ford Fusion ", costs = 160000, description = {}, model = "fusiont"},	
				{name = "Mercedes-Benz AMG E63s ", costs = 350000, description = {}, model = "e63w213"},
			   {name = "Asea", costs = 1000000, description = {}, model = "asea"},
			    {name = "Asterope", costs = 1000000, description = {}, model = "asterope"},
			    {name = "Cognoscenti", costs = 1000000, description = {}, model = "cognoscenti"},
			    {name = "Cognoscenti(Armored)", costs = 1000000, description = {}, model = "cognoscenti2"},
			    {name = "Cognoscenti 55", costs = 1000000, description = {}, model = "cog55"},
			    {name = "Cognoscenti 55(Armored", costs = 1500000, description = {}, model = "cog552"},
			    {name = "Fugitive", costs = 24000, description = {}, model = "fugitive"},
			    {name = "Glendale", costs = 200000, description = {}, model = "glendale"},
			    {name = "Ingot", costs = 9000, description = {}, model = "ingot"},
			    {name = "Intruder", costs = 16000, description = {}, model = "intruder"},
			    {name = "Premier", costs = 10000, description = {}, model = "premier"},
			    {name = "Primo", costs = 9000, description = {}, model = "primo"},
			    {name = "Primo Custom", costs = 9500, description = {}, model = "primo2"},
			    {name = "Regina", costs = 8000, description = {}, model = "regina"},
			    {name = "Schafter", costs = 65000, description = {}, model = "schafter2"},
			    {name = "Stanier", costs = 10000, description = {}, model = "stanier"},
			    {name = "Stratum", costs = 10000, description = {}, model = "stratum"},
			    {name = "Stretch", costs = 30000, description = {}, model = "stretch"},
			    {name = "Super Diamond", costs = 250000, description = {}, model = "superd"},
			    {name = "Surge", costs = 38000, description = {}, model = "surge"},
			    {name = "Tailgater", costs = 55000, description = {}, model = "tailgater"},
			    {name = "Warrener", costs = 120000, description = {}, model = "warrener"},
			    {name = "Washington", costs = 15000, description = {}, model = "washington"},--]]
			}
		},
		["caminhao"] = {
			title = "Caminhão",
			name = "caminhao",
			buttons = {				
			--[[ 	{name = "Daf XF 105", costs = 381000, description = {}, model = "daf"},
				{name = "MAN TGX V8 Euro 5", costs = 421000, description = {}, model = "man"},
				{name = "Constellation", costs = 80000, description = {}, model = "hauler"},]]
			}
		},
		["drift"] = {
			title = "Drift",
			name = "drift",
			buttons = {			
			--[[ 	{name = "Constellation", costs = 80000, description = {}, model = "hauler"},	
				{name = "Honda Civic Type R EK9", costs = 381000, description = {}, model = "EK9"},
				{name = "Lancer Evolution X", costs = 421000, description = {}, model = "evo10"},
				{name = "Lexus Sc 300", costs = 371000, description = {}, model = "sc300"},
				{name = "Mazda RX 7 FC3", costs = 371000, description = {}, model = "rx7twerk"},
				{name = "Mazda RX7 202 Spirit", costs = 371000, description = {}, model = "fd"},
				{name = "Nissan Silvia", costs = 381000, description = {}, model = "s15rb"},
				{name = "Nissan Skyline ER 34", costs = 50000000, description = {}, model = "gtr"},
				{name = "Nissan GTR", costs = 50000000, description = {}, model = "d1r34"},
				{name = "Subaru GT", costs = 50000000, description = {}, model = "SubaruGT"},
				{name = "Nissan Civic SIR", costs = 50000000, description = {}, model = "eg6"},
				{name = "Nissan Skyline", costs = 50000000, description = {}, model = "skyline"},]]
			}
		},
		----------------- MOTOS-----------------
		["scooter"] = {
			title = "Scooter",
			name = "scooter",
			buttons = {
			 --[[   {name = "Bagger", costs = 5000, description = {}, model = "bagger"},
			    {name = "Akuma", costs = 9000, description = {}, model = "AKUMA"},
			    {name = "Bati 801", costs = 15000, description = {}, model = "bati"},
			    {name = "Bati 801RR", costs = 15000, description = {}, model = "bati2"},
			    {name = "BF400", costs = 95000, description = {}, model = "bf400"},
			    {name = "Carbon RS", costs = 40000, description = {}, model = "carbonrs"},
			    {name = "Cliffhanger", costs = 225000, description = {}, model = "cliffhanger"},
			    {name = "Daemon", costs = 5000, description = {}, model = "daemon"},
			    {name = "Double T", costs = 12000, description = {}, model = "double"},
			    {name = "Enduro", costs = 48000, description = {}, model = "enduro"},
			    {name = "Faggio", costs = 4000, description = {}, model = "faggio2"},
			    {name = "Gargoyle", costs = 120000, description = {}, model = "gargoyle"},
			    {name = "Hakuchou", costs = 82000, description = {}, model = "hakuchou"},
			    {name = "Hexer", costs = 15000, description = {}, model = "hexer"},
			    {name = "Innovation", costs = 90000, description = {}, model = "innovation"},
			    {name = "Lectro", costs = 700000, description = {}, model = "lectro"},
			    {name = "Nemesis", costs = 12000, description = {}, model = "nemesis"},
			    {name = "PCJ-600", costs = 9000, description = {}, model = "pcj"},
			    {name = "Ruffian", costs = 9000, description = {}, model = "ruffian"},
			    {name = "Sanchez", costs = 7000, description = {}, model = "sanchez"},
			    {name = "Sovereign", costs = 90000, description = {}, model = "sovereign"},
			    {name = "Thrust", costs = 75000, description = {}, model = "thrust"},
			    {name = "Vader", costs = 9000, description = {}, model = "vader"},
			    {name = "Vindicator", costs = 600000, description = {}, model = "vindicator"},--]]
			}
		},
		["rua"] = {
			title = "Motos de Rua",
			name = "rua",
			buttons = {
			 --[[   {name = "xt66", costs = 84641, description = {}, model = "Yamaha XT660R Enduro"},
			    {name = "titan60", costs = 84641, description = {}, model = "Honda Titan 160"},
			    {name = "Akuma", costs = 9000, description = {}, model = "AKUMA"},
			    {name = "Bagger", costs = 5000, description = {}, model = "bagger"},
			    {name = "Bati 801", costs = 15000, description = {}, model = "bati"},
			    {name = "Bati 801RR", costs = 15000, description = {}, model = "bati2"},
			    {name = "BF400", costs = 95000, description = {}, model = "bf400"},
			    {name = "Carbon RS", costs = 40000, description = {}, model = "carbonrs"},
			    {name = "Cliffhanger", costs = 225000, description = {}, model = "cliffhanger"},
			    {name = "Daemon", costs = 5000, description = {}, model = "daemon"},
			    {name = "Double T", costs = 12000, description = {}, model = "double"},
			    {name = "Enduro", costs = 48000, description = {}, model = "enduro"},
			    {name = "Faggio", costs = 4000, description = {}, model = "faggio2"},
			    {name = "Gargoyle", costs = 120000, description = {}, model = "gargoyle"},
			    {name = "Hakuchou", costs = 82000, description = {}, model = "hakuchou"},
			    {name = "Hexer", costs = 15000, description = {}, model = "hexer"},
			    {name = "Innovation", costs = 90000, description = {}, model = "innovation"},
			    {name = "Lectro", costs = 700000, description = {}, model = "lectro"},
			    {name = "Nemesis", costs = 12000, description = {}, model = "nemesis"},
			    {name = "PCJ-600", costs = 9000, description = {}, model = "pcj"},
			    {name = "Ruffian", costs = 9000, description = {}, model = "ruffian"},
			    {name = "Sanchez", costs = 7000, description = {}, model = "sanchez"},
			    {name = "Sovereign", costs = 90000, description = {}, model = "sovereign"},
			    {name = "Thrust", costs = 75000, description = {}, model = "thrust"},
			    {name = "Vader", costs = 9000, description = {}, model = "vader"},
			    {name = "Vindicator", costs = 600000, description = {}, model = "vindicator"}, ]]
			}
		},
		["trilha"] = {
			title = "offroad",
			name = "trilha",
			buttons = {
			--[[    {name = "rc", costs = 53100, description = {}, model = "KTM  RC390 - 2014"},
			    {name = "Akuma", costs = 9000, description = {}, model = "AKUMA"},
			    {name = "Bagger", costs = 5000, description = {}, model = "bagger"},
			    {name = "Bati 801", costs = 15000, description = {}, model = "bati"},
			    {name = "Bati 801RR", costs = 15000, description = {}, model = "bati2"},
			    {name = "BF400", costs = 95000, description = {}, model = "bf400"},
			    {name = "Carbon RS", costs = 40000, description = {}, model = "carbonrs"},
			    {name = "Cliffhanger", costs = 225000, description = {}, model = "cliffhanger"},
			    {name = "Daemon", costs = 5000, description = {}, model = "daemon"},
			    {name = "Double T", costs = 12000, description = {}, model = "double"},
			    {name = "Enduro", costs = 48000, description = {}, model = "enduro"},
			    {name = "Faggio", costs = 4000, description = {}, model = "faggio2"},
			    {name = "Gargoyle", costs = 120000, description = {}, model = "gargoyle"},
			    {name = "Hakuchou", costs = 82000, description = {}, model = "hakuchou"},
			    {name = "Hexer", costs = 15000, description = {}, model = "hexer"},
			    {name = "Innovation", costs = 90000, description = {}, model = "innovation"},
			    {name = "Lectro", costs = 700000, description = {}, model = "lectro"},
			    {name = "Nemesis", costs = 12000, description = {}, model = "nemesis"},
			    {name = "PCJ-600", costs = 9000, description = {}, model = "pcj"},
			    {name = "Ruffian", costs = 9000, description = {}, model = "ruffian"},
			    {name = "Sanchez", costs = 7000, description = {}, model = "sanchez"},
			    {name = "Sovereign", costs = 90000, description = {}, model = "sovereign"},
			    {name = "Thrust", costs = 75000, description = {}, model = "thrust"},
			    {name = "Vader", costs = 9000, description = {}, model = "vader"},
			    {name = "Vindicator", costs = 600000, description = {}, model = "vindicator"}, ]]
			}
		},
		["esportiva"] = {
			title = "Esportivas",
			name = "esportiva",
			buttons = {
			--[[    {name = "BMW S1000RR", costs = 91000, description = {}, model = "bmws"},
			    {name = "Honda CBR 1000RR", costs = 89100, description = {}, model = "hcbr17"},
			    {name = "nh2r", costs = 378100, description = {}, model = "Kawasaki Ninja H2R"},
			    {name = "zx10r", costs = 84641, description = {}, model = "Kawasaki ZX10R"},
			    {name = "r1v2", costs = 80950, description = {}, model = "Yamaha YZF R1 2015"},
			    {name = "r6", costs = 67100, description = {}, model = "Yamaha R6 - 2017"},
			    {name = "mt10", costs = 78100, description = {}, model = "Yamaha MT10"},
			    {name = "Akuma", costs = 9000, description = {}, model = "AKUMA"},
			    {name = "Bagger", costs = 5000, description = {}, model = "bagger"},
			    {name = "Bati 801", costs = 15000, description = {}, model = "bati"},
			    {name = "Bati 801RR", costs = 15000, description = {}, model = "bati2"},
			    {name = "BF400", costs = 95000, description = {}, model = "bf400"},
			    {name = "Carbon RS", costs = 40000, description = {}, model = "carbonrs"},
			    {name = "Cliffhanger", costs = 225000, description = {}, model = "cliffhanger"},
			    {name = "Daemon", costs = 5000, description = {}, model = "daemon"},
			    {name = "Double T", costs = 12000, description = {}, model = "double"},
			    {name = "Enduro", costs = 48000, description = {}, model = "enduro"},
			    {name = "Faggio", costs = 4000, description = {}, model = "faggio2"},
			    {name = "Gargoyle", costs = 120000, description = {}, model = "gargoyle"},
			    {name = "Hakuchou", costs = 82000, description = {}, model = "hakuchou"},
			    {name = "Hexer", costs = 15000, description = {}, model = "hexer"},
			    {name = "Innovation", costs = 90000, description = {}, model = "innovation"},
			    {name = "Lectro", costs = 700000, description = {}, model = "lectro"},
			    {name = "Nemesis", costs = 12000, description = {}, model = "nemesis"},
			    {name = "PCJ-600", costs = 9000, description = {}, model = "pcj"},
			    {name = "Ruffian", costs = 9000, description = {}, model = "ruffian"},
			    {name = "Sanchez", costs = 7000, description = {}, model = "sanchez"},
			    {name = "Sovereign", costs = 90000, description = {}, model = "sovereign"},
			    {name = "Thrust", costs = 75000, description = {}, model = "thrust"},
			    {name = "Vader", costs = 9000, description = {}, model = "vader"},
			    {name = "Vindicator", costs = 600000, description = {}, model = "vindicator"},]]
			}
		},
		["mvips"] = {
			title = "Vips",
			name = "mvips",
			buttons = {
			--[[    {name = "dm1200", costs = 5000000, description = {}, model = "Ducatti Multilistrada"},
			    {name = "d99", costs = 5000000, description = {}, model = "Ducatti 1199 Panigalle"},
			    {name = "Akuma", costs = 9000, description = {}, model = "AKUMA"},
			    {name = "Bagger", costs = 5000, description = {}, model = "bagger"},
			    {name = "Bati 801", costs = 15000, description = {}, model = "bati"},
			    {name = "Bati 801RR", costs = 15000, description = {}, model = "bati2"},
			    {name = "BF400", costs = 95000, description = {}, model = "bf400"},
			    {name = "Carbon RS", costs = 40000, description = {}, model = "carbonrs"},
			    {name = "Cliffhanger", costs = 225000, description = {}, model = "cliffhanger"},
			    {name = "Daemon", costs = 5000, description = {}, model = "daemon"},
			    {name = "Double T", costs = 12000, description = {}, model = "double"},
			    {name = "Enduro", costs = 48000, description = {}, model = "enduro"},
			    {name = "Faggio", costs = 4000, description = {}, model = "faggio2"},
			    {name = "Gargoyle", costs = 120000, description = {}, model = "gargoyle"},
			    {name = "Hakuchou", costs = 82000, description = {}, model = "hakuchou"},
			    {name = "Hexer", costs = 15000, description = {}, model = "hexer"},
			    {name = "Innovation", costs = 90000, description = {}, model = "innovation"},
			    {name = "Lectro", costs = 700000, description = {}, model = "lectro"},
			    {name = "Nemesis", costs = 12000, description = {}, model = "nemesis"},
			    {name = "PCJ-600", costs = 9000, description = {}, model = "pcj"},
			    {name = "Ruffian", costs = 9000, description = {}, model = "ruffian"},
			    {name = "Sanchez", costs = 7000, description = {}, model = "sanchez"},
			    {name = "Sovereign", costs = 90000, description = {}, model = "sovereign"},
			    {name = "Thrust", costs = 75000, description = {}, model = "thrust"},
			    {name = "Vader", costs = 9000, description = {}, model = "vader"},
			    {name = "Vindicator", costs = 600000, description = {}, model = "vindicator"},
			}--]]
		},
	}
}
local fakecar = {model = '', car = nil}
local vehshop_locations = {
{entering = {-33.803,-1102.322,25.422}, inside = {-49.414443969727,-1110.2772216797,26.435815811157}, outside = {-62.949256896973,-1100.1599121094,26.270923614502}},
}

local vehshop_blips ={}
local inrangeofvehshop = false
local currentlocation = nil
local boughtcar = false

function vehSR_drawTxt(text,font,centre,x,y,scale,r,g,b,a)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(centre)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x , y)
end

function vehSR_IsPlayerInRangeOfVehshop()
	return inrangeofvehshop
end

local firstspawn = 0
AddEventHandler('playerSpawned', function(spawn)
if firstspawn == 0 then
	--326 car blip 227 225
	vehSR_ShowVehshopBlips(true)
	firstspawn = 1
end
end)

function vehSR_ShowVehshopBlips(bool)
	if bool and #vehshop_blips == 0 then
		for station,pos in pairs(vehshop_locations) do
			local loc = pos
			pos = pos.entering
			local blip = AddBlipForCoord(pos[1],pos[2],pos[3])
			-- 60 58 137
			SetBlipSprite(blip,326)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString("Simeon Showroom")
			EndTextCommandSetBlipName(blip)
			SetBlipAsShortRange(blip,true)
			SetBlipAsMissionCreatorBlip(blip,true)
			table.insert(vehshop_blips, {blip = blip, pos = loc})
		end
		Citizen.CreateThread(function()
			while #vehshop_blips > 0 do
				Citizen.Wait(0)
				local inrange = false
				for i,b in ipairs(vehshop_blips) do
					if IsPlayerWantedLevelGreater(GetPlayerIndex(),0) == false and vehshop.opened == false and IsPedInAnyVehicle(vehSR_LocalPed(), true) == false and  GetDistanceBetweenCoords(b.pos.entering[1],b.pos.entering[2],b.pos.entering[3],GetEntityCoords(vehSR_LocalPed())) < 5 then
						DrawMarker(1,b.pos.entering[1],b.pos.entering[2],b.pos.entering[3],0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
						vehSR_drawTxt("Pressione ~g~ENTER~s~ para comprar ~b~veiculos",0,1,0.5,0.8,0.6,255,255,255,255)
						currentlocation = b
						inrange = true
					end
				end
				inrangeofvehshop = inrange
			end
		end)
	elseif bool == false and #vehshop_blips > 0 then
		for i,b in ipairs(vehshop_blips) do
			if DoesBlipExist(b.blip) then
				SetBlipAsMissionCreatorBlip(b.blip,false)
				Citizen.InvokeNative(0x86A652570E5F25DD, Citizen.PointerValueIntInitialized(b.blip))
			end
		end
		vehshop_blips = {}
	end
end

function vehSR_f(n)
	return n + 0.0001
end

function vehSR_LocalPed()
	return GetPlayerPed(-1)
end

function vehSR_try(f, catch_f)
	local status, exception = pcall(f)
	if not status then
		catch_f(exception)
	end
end
function vehSR_firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end
--local veh = nil
function vehSR_OpenCreator()
	boughtcar = false
	local ped = vehSR_LocalPed()
	local pos = currentlocation.pos.inside
	FreezeEntityPosition(ped,true)
	SetEntityVisible(ped,false)
	local g = Citizen.InvokeNative(0xC906A7DAB05C8D2B,pos[1],pos[2],pos[3],Citizen.PointerValueFloat(),0)
	SetEntityCoords(ped,pos[1],pos[2],g)
	SetEntityHeading(ped,pos[4])
	vehshop.currentmenu = "main"
	vehshop.opened = true
	vehshop.selectedbutton = 0
end
local vehicle_price = 0
function vehSR_CloseCreator(vehicle,veh_type)
	Citizen.CreateThread(function()
		local ped = vehSR_LocalPed()
		if not boughtcar then
			local pos = currentlocation.pos.entering
			SetEntityCoords(ped,pos[1],pos[2],pos[3])
			FreezeEntityPosition(ped,false)
			SetEntityVisible(ped,true)
		else
			deleteVehiclePedIsIn()
			vRP.teleport({-44.21378326416,-1079.1402587891,26.67050743103})
			vRPg.spawnBoughtVehicle({veh_type, vehicle})
			SetEntityVisible(ped,true)
			FreezeEntityPosition(ped,false)
		end
		vehshop.opened = false
		vehshop.menu.from = 1
		vehshop.menu.to = 10
	end)
end

function vehSR_drawMenuButton(button,x,y,selected)
	local menu = vehshop.menu
	SetTextFont(menu.font)
	SetTextProportional(0)
	SetTextScale(menu.scale, menu.scale)
	if selected then
		SetTextColour(0, 0, 0, 255)
	else
		SetTextColour(255, 255, 255, 255)
	end
	SetTextCentre(0)
	SetTextEntry("STRING")
	AddTextComponentString(button.name)
	if selected then
		DrawRect(x,y,menu.width,menu.height,255,255,255,255)
	else
		DrawRect(x,y,menu.width,menu.height,0,0,0,150)
	end
	DrawText(x - menu.width/2 + 0.005, y - menu.height/2 + 0.0028)
end

function vehSR_drawMenuTitle(txt,x,y)
local menu = vehshop.menu
	SetTextFont(2)
	SetTextProportional(0)
	SetTextScale(0.5, 0.5)
	SetTextColour(255, 255, 255, 255)
	SetTextEntry("STRING")
	AddTextComponentString(txt)
	DrawRect(x,y,menu.width,menu.height,0,0,0,150)
	DrawText(x - menu.width/2 + 0.005, y - menu.height/2 + 0.0028)
end
function vehSR_tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end
function vehSR_Notify(text)
SetNotificationTextEntry('STRING')
AddTextComponentString(text)
DrawNotification(false, false)
end

function vehSR_drawMenuRight(txt,x,y,selected)
	local menu = vehshop.menu
	SetTextFont(menu.font)
	SetTextProportional(0)
	SetTextScale(menu.scale, menu.scale)
	if selected then
		SetTextColour(0, 0, 0, 255)
	else
		SetTextColour(255, 255, 255, 255)
	end
	SetTextCentre(0)
	SetTextEntry("STRING")
	AddTextComponentString(txt)
	DrawText(x + menu.width/2 - 0.06, y - menu.height/2 + 0.0028)
end
local backlock = false
Citizen.CreateThread(function()
	local last_dir
	while true do
		Citizen.Wait(0)
		if IsControlJustPressed(1,201) and vehSR_IsPlayerInRangeOfVehshop() then
			if vehshop.opened then
				vehSR_CloseCreator("","")
			else
				vehSR_OpenCreator()
			end
		end
		if vehshop.opened then
			local ped = vehSR_LocalPed()
			local menu = vehshop.menu[vehshop.currentmenu]
			vehSR_drawTxt(vehshop.title,1,1,vehshop.menu.x,vehshop.menu.y,1.0, 255,255,255,255)
			vehSR_drawMenuTitle(menu.title, vehshop.menu.x,vehshop.menu.y + 0.08)
			vehSR_drawTxt(vehshop.selectedbutton.."/"..vehSR_tablelength(menu.buttons),0,0,vehshop.menu.x + vehshop.menu.width/2 - 0.0385,vehshop.menu.y + 0.067,0.4, 255,255,255,255)
			local y = vehshop.menu.y + 0.12
			buttoncount = vehSR_tablelength(menu.buttons)
			local selected = false

			for i,button in pairs(menu.buttons) do
				if i >= vehshop.menu.from and i <= vehshop.menu.to then

					if i == vehshop.selectedbutton then
						selected = true
					else
						selected = false
					end
					vehSR_drawMenuButton(button,vehshop.menu.x,y,selected)
					if button.costs ~= nil then
						if vehshop.currentmenu == "compacto" or vehshop.currentmenu == "cupe" or vehshop.currentmenu == "drift" or vehshop.currentmenu == "caminhao" or vehshop.currentmenu == "populares" or vehshop.currentmenu == "vips" or vehshop.currentmenu == "scooter" or vehshop.currentmenu == "trilha" or vehshop.currentmenu == "esportiva" or vehshop.currentmenu == "mvip" or vehshop.currentmenu == "sedans" or vehshop.currentmenu == "esportivo" or vehshop.currentmenu == "esportivoclasico" or vehshop.currentmenu == "supers" or vehshop.currentmenu == "muscle" or vehshop.currentmenu == "offroad" or vehshop.currentmenu == "suvs" or vehshop.currentmenu == "vans" or vehshop.currentmenu == "industrial" or vehshop.currentmenu == "cycles" or vehshop.currentmenu == "motorcycles" then
							vehSR_drawMenuRight(button.costs.."$",vehshop.menu.x,y,selected)
						else
							vehSR_drawMenuButton(button,vehshop.menu.x,y,selected)
						end
					end
					y = y + 0.04
					if vehshop.currentmenu == "compacto" or vehshop.currentmenu == "cupe" or vehshop.currentmenu == "drift" or vehshop.currentmenu == "caminhao" or vehshop.currentmenu == "populares" or vehshop.currentmenu == "vips" or vehshop.currentmenu == "scooter" or vehshop.currentmenu == "trilha" or vehshop.currentmenu == "esportiva" or vehshop.currentmenu == "mvip" or vehshop.currentmenu == "sedans" or vehshop.currentmenu == "esportivo" or vehshop.currentmenu == "esportivoclasico" or vehshop.currentmenu == "supers" or vehshop.currentmenu == "muscle" or vehshop.currentmenu == "offroad" or vehshop.currentmenu == "suvs" or vehshop.currentmenu == "vans" or vehshop.currentmenu == "industrial" or vehshop.currentmenu == "cycles" or vehshop.currentmenu == "motorcycles" then
						if selected then
							if fakecar.model ~= button.model then
								if DoesEntityExist(fakecar.car) then
									Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(fakecar.car))
								end
								local pos = currentlocation.pos.inside
								local hash = GetHashKey(button.model)
								RequestModel(hash)
								local timer = 0
								while not HasModelLoaded(hash) and timer < 255 do
									Citizen.Wait(1)
									vehSR_drawTxt("Carregando...",0,1,0.5,0.5,1.5,255,255-timer,255-timer,255)
									RequestModel(hash)
									timer = timer + 1
								end
								if timer < 255 then
									local veh = CreateVehicle(hash,pos[1],pos[2],pos[3],pos[4],false,false)
									while not DoesEntityExist(veh) do
										Citizen.Wait(1)
										vehSR_drawTxt("Carregando...",0,1,0.5,0.5,1.5,255,255-timer,255-timer,255)
									end
									FreezeEntityPosition(veh,true)
									SetEntityInvincible(veh,true)
									SetVehicleDoorsLocked(veh,4)
									--SetEntityCollision(veh,false,false)
									TaskWarpPedIntoVehicle(vehSR_LocalPed(),veh,-1)
									for i = 0,24 do
										SetVehicleModKit(veh,0)
										RemoveVehicleMod(veh,i)
									end
									fakecar = { model = button.model, car = veh}
								else
									timer = 0
									while timer < 50 do
										Citizen.Wait(1)
										vehSR_drawTxt("Failed!",0,1,0.5,0.5,1.5,255,0,0,255)
										timer = timer + 1
									end
									if last_dir then
										if vehshop.selectedbutton < buttoncount then
											vehshop.selectedbutton = vehshop.selectedbutton +1
											if buttoncount > 10 and vehshop.selectedbutton > vehshop.menu.to then
												vehshop.menu.to = vehshop.menu.to + 1
												vehshop.menu.from = vehshop.menu.from + 1
											end
										else
											last_dir = false
											vehshop.selectedbutton = vehshop.selectedbutton -1
											if buttoncount > 10 and vehshop.selectedbutton < vehshop.menu.from then
												vehshop.menu.from = vehshop.menu.from -1
												vehshop.menu.to = vehshop.menu.to - 1
											end
										end
									else
										if vehshop.selectedbutton > 1 then
											vehshop.selectedbutton = vehshop.selectedbutton -1
											if buttoncount > 10 and vehshop.selectedbutton < vehshop.menu.from then
												vehshop.menu.from = vehshop.menu.from -1
												vehshop.menu.to = vehshop.menu.to - 1
											end
										else
											last_dir = true
											vehshop.selectedbutton = vehshop.selectedbutton +1
											if buttoncount > 10 and vehshop.selectedbutton > vehshop.menu.to then
												vehshop.menu.to = vehshop.menu.to + 1
												vehshop.menu.from = vehshop.menu.from + 1
											end
										end
									end
								end
							end
						end
					end
					if selected and IsControlJustPressed(1,201) then
						vehSR_ButtonSelected(button)
					end
				end
			end
			if IsControlJustPressed(1,202) then
				vehSR_Back()
			end
			if IsControlJustReleased(1,202) then
				backlock = false
			end
			if IsControlJustPressed(1,188) then
				last_dir = false
				if vehshop.selectedbutton > 1 then
					vehshop.selectedbutton = vehshop.selectedbutton -1
					if buttoncount > 10 and vehshop.selectedbutton < vehshop.menu.from then
						vehshop.menu.from = vehshop.menu.from -1
						vehshop.menu.to = vehshop.menu.to - 1
					end
				end
			end
			if IsControlJustPressed(1,187)then
				last_dir = true
				if vehshop.selectedbutton < buttoncount then
					vehshop.selectedbutton = vehshop.selectedbutton +1
					if buttoncount > 10 and vehshop.selectedbutton > vehshop.menu.to then
						vehshop.menu.to = vehshop.menu.to + 1
						vehshop.menu.from = vehshop.menu.from + 1
					end
				end
			end
		end

	end
end)


function vehSR_round(num, idp)
  if idp and idp>0 then
    local mult = 10^idp
    return math.floor(num * mult + 0.5) / mult
  end
  return math.floor(num + 0.5)
end
function vehSR_ButtonSelected(button)
	local ped = GetPlayerPed(-1)
	local this = vehshop.currentmenu
	local btn = button.name
	if this == "main" then
		if btn == "vehicles" then
			vehSR_OpenMenu('vehicles')
		elseif btn == "motorcycles" then
			vehSR_OpenMenu('motorcycles')
		end
	elseif this == "vehicles" then
		if btn == "esportivo" then
			vehSR_OpenMenu('esportivo')
		elseif btn == "sedans" then
			vehSR_OpenMenu('sedans')
		elseif btn == "populares" then
			vehSR_OpenMenu('populares')
		elseif btn == "caminhao" then
			vehSR_OpenMenu('caminhao')
		elseif btn == "drift" then
			vehSR_OpenMenu('drift')
		elseif btn == "vips" then
			vehSR_OpenMenu('vips')
		elseif btn == "scooter" then
			vehSR_OpenMenu('scooter')
		elseif btn == "rua" then
			vehSR_OpenMenu('rua')
		elseif btn == "trilha" then
			vehSR_OpenMenu('trilha')
		elseif btn == "esportiva" then
			vehSR_OpenMenu('esportiva')
		elseif btn == "mvips" then
			vehSR_OpenMenu('mvips')
		elseif btn == "compacto" then
			vehSR_OpenMenu('compacto')
		elseif btn == "cupe" then
			vehSR_OpenMenu('cupe')
		elseif btn == "esportivoclasico" then
			vehSR_OpenMenu("esportivoclasico")
		elseif btn == "supers" then
			vehSR_OpenMenu('supers')
		elseif btn == "muscle" then
			vehSR_OpenMenu('muscle')
		elseif btn == "offroad" then
			vehSR_OpenMenu('offroad')
		elseif btn == "suvs" then
			vehSR_OpenMenu('suvs')
		elseif btn == "vans" then
			vehSR_OpenMenu('vans')
		end
	elseif this == "compacto" or this == "cupe" or this == "drift" or this == "caminhao" or this == "populares" or this == "vips" or this == "sedans" or this == "esportivo" or this == "esportivoclasico" or this == "supers" or this == "muscle" or this == "offroad" or this == "suvs" or this == "vans" or this == "industrial" then
		TriggerServerEvent('veh_SR:CheckMoneyForVeh',button.model,button.costs, "car")
    elseif this == "cycles" or this == "motorcycles" or this == "scooter" or this == "rua" or this == "trilha" or this == "esportiva" or this == "mvips" then
		TriggerServerEvent('veh_SR:CheckMoneyForVeh',button.model,button.costs, "bike")
	end
end

RegisterNetEvent('veh_SR:CloseMenu')
AddEventHandler('veh_SR:CloseMenu', function(vehicle, veh_type)
	boughtcar = true
	vehSR_CloseCreator(vehicle,veh_type)
end)

function vehSR_OpenMenu(menu)
	fakecar = {model = '', car = nil}
	vehshop.lastmenu = vehshop.currentmenu
	if menu == "vehicles" then
		vehshop.lastmenu = "main"
	elseif menu == "bikes"  then
		vehshop.lastmenu = "main"
	elseif menu == 'race_create_objects' then
		vehshop.lastmenu = "main"
	elseif menu == "race_create_objects_spawn" then
		vehshop.lastmenu = "race_create_objects"
	end
	vehshop.menu.from = 1
	vehshop.menu.to = 10
	vehshop.selectedbutton = 0
	vehshop.currentmenu = menu
end


function vehSR_Back()
	if backlock then
		return
	end
	backlock = true
	if vehshop.currentmenu == "main" then
		vehSR_CloseCreator("","")
	elseif vehshop.currentmenu == "compacto" or vehshop.currentmenu == "cupe" or vehshop.currentmenu == "drift" or vehshop.currentmenu == "caminhao" or vehshop.currentmenu == "sedans" or vehshop.currentmenu == "populares" or vehshop.currentmenu == "vips" or vehshop.currentmenu == "scooter" or vehshop.currentmenu == "trilha" or vehshop.currentmenu == "esportiva" or vehshop.currentmenu == "mvip" or vehshop.currentmenu == "sedans" or vehshop.currentmenu == "esportivo" or vehshop.currentmenu == "esportivoclasico" or vehshop.currentmenu == "supers" or vehshop.currentmenu == "muscle" or vehshop.currentmenu == "offroad" or vehshop.currentmenu == "suvs" or vehshop.currentmenu == "vans" or vehshop.currentmenu == "industrial" or vehshop.currentmenu == "cycles" or vehshop.currentmenu == "motorcycles" then
		if DoesEntityExist(fakecar.car) then
			Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(fakecar.car))
		end
		fakecar = {model = '', car = nil}
		vehSR_OpenMenu(vehshop.lastmenu)
	else
		vehSR_OpenMenu(vehshop.lastmenu)
	end

end

function vehSR_stringstarts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end