local lang = {
  blip = "Roubo",
  cops = {
    cant_rob = "~r~Policiais não podem roubar lojas!",
	not_enough = "~r~Voce precisa pelomenos {1} policias online"
  },
  robbery = {
	wait = "Aqui já foi robado recentemente!. Porfavor esperer: ^2{1}^0 segundos.",
	progress = "Roubo in Progesso ^2{1}",
	started = "Voce inicou um roubo: ^2{1}^0, não se afaste desse ponto!",
	hold = "Segure por ^1{1} ^0minutos e o dinheiro será seu!",
	over = "Robbery is over at: ^2{1}^0!",
	canceled = "Roubo foi cancelado em!: ^2{1}^0!",
	done = "Roubo Concluido! voce recebeu: ^2{1}^0!"
  },
  title = {
    robbery = "[Roubo]",
	news = "[Brasil New Life Noticias]",
	system = "[Brasil New Life]"
  },
  client = {
	rob = "Pressione ~INPUT_RELOAD~ para roubar ~b~{1}~w~ Cuidado, a policia será alertada!",
    robbing = "Roubo: ~r~{1}~w~ segundos !",
	canceled = "Roubo foi cancelado, você nao receberar nada!."
  }
}

return lang