-- Change key on client.lua, default key is PAGE UP

cfg = {
  { -- Default distance to voice
    distance = 8.001,
    text = "Normal", 
    css = [[
      .div_voice_text{
        position: absolute;
        top: 50px;
        right: 0px;
        font-size: 20px;
        font-weight: bold;
        color: white;
        text-shadow: 3px 3px 2px rgba(0, 0, 0, 0.80);
	  }
    ]]
  },
  { -- High distance to voice
    distance = 35.001,
    text = "Alto",
    css = [[
      .div_voice_text{
        position: absolute;
        top: 50px;
        right: 0px;
        font-size: 20px;
        font-weight: bold;
        color: white;
        text-shadow: 3px 3px 2px rgba(0, 0, 0, 0.80);
	  }
    ]]
  },
  { -- Low distance to voice
    distance = 3.001,
    text = "Sussuro",
    css = [[
      .div_voice_text{
        position: absolute;
        top: 50px;
        right: 0px;
        font-size: 20px;
        font-weight: bold;
        color: white;
        text-shadow: 3px 3px 2px rgba(0, 0, 0, 0.80);
	  }
    ]]
  }
}

return cfg